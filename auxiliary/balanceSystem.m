function [Ab,Bb,Cb,Db,Winv] = balanceSystem(A,B,C,D)
N = size(A,1);

% compute P and Q
Q = discrete_lyapunov_equation(A',(C')*C);
P = discrete_lyapunov_equation(A ,B*(B'));
% when the system is balanced, check that P and Q are equal and diagonal
T = P-Q;
if any(abs(T(:))>1e-12)
%     sys = ss(A,B,C,D,-1);
%     [sys,g] = balreal(sys);
%     [Ab,Bb,Cb,Db]=ssdata(sys);
    
    [Ab,Bb,Cb,Db,g] = balance(A,B,C,D,P,Q);
    
%     [Abt Bbt;Cbt Dbt] - [Ab Bb;Cb Db]
%     Q = discrete_lyapunov_equation(Abt',(Cbt')*Cbt);
%     P = discrete_lyapunov_equation(Abt ,Bbt*(Bbt'));
%     T = P-Q
    
else
    Ab=A;
    Bb=B;
    Cb=C;
    Db=D;
    g = diag(Q);
end

Winv = diag(1./g);

% % compare the two systems
% theta = linspace(0,2*pi,1001);
% Go = evaluateSystem(A ,B ,C ,D ,theta);
% Gb = evaluateSystem(Ab,Bb,Cb,Db,theta);
% err = max(abs(Go(:)-Gb(:)));
% keyboard

end

function P = discrete_lyapunov_equation(A, Q)
% This function solves A * P * A' - P + Q = 0
% Based on https://math.stackexchange.com/questions/2483215/how-to-solve-lyapunov-equation-via-matlab-octave
% Improved by David
P = reshape((eye(numel(A)) - kron(conj(A),A)) \ Q(:), size(A));
end

function [Ab,Bb,Cb,Db,g] = balance(A,B,C,D,P,Q)
% Rr = dlyapchol(A ,B ,[],'noscale');
% Ro = dlyapchol(A',C',[],'noscale');
Ro = chol((Q+Q')/2);
Rr = chol((P+P')/2);
% try
%     Ro = chol((Q+Q')/2);
%     Rr = chol((P+P')/2);
% catch err
%     keyboard
% end
[u,s,v] = svd(Ro*Rr');
g = diag(s);
sgi = 1./sqrt(g);
Ts = repmat(sgi,[1 size(A,1)]) .* (u'*Ro);
Tsi = (Rr'*v) .* repmat(sgi.',[size(A,1) 1]);
Ab = Ts*A*Tsi;
Bb = Ts*B;
Cb = C*Tsi;
Db = D;
end

function res = evaluateSystem(A,B,C,D,theta)
res = zeros(size(D,1),size(D,2),numel(theta));
for ff=1:numel(theta)
    res(:,:,ff) = C*((exp(1i*theta(ff)).*eye(size(A,1))-A)\B);
end
res = res+D;
end