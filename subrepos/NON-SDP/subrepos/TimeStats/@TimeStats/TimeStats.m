% Statistics object for penlab
classdef TimeStats < handle
    properties (GetAccess=public, SetAccess=private)
        created;
        numcalls;  % Structure, number of calls of each function
        evaltime;  % Structure, total time spent in each function       
    end
     properties (GetAccess=private, SetAccess=private)
        timer;     % Internal clock for each function.
     end   
    properties (Dependent)
       meantime; 
    end
    methods
        function obj = TimeStats()
           obj.created = datestr(now); 
        end
        function init(obj)
            obj.created = datestr(now);
            obj.numcalls = [];
            obj.evaltime = [];
            obj.timer = [];
        end
        function start(obj,LABEL) % Start timer
            if ~isfield(obj.timer,LABEL)
                obj.numcalls.(LABEL) = 0;
                obj.evaltime.(LABEL) = 0;
            end
            obj.timer.(LABEL) = tic;
        end
        function finish(obj,LABEL) % Stop timer
            obj.numcalls.(LABEL) = obj.numcalls.(LABEL) + 1;
            obj.evaltime.(LABEL) = obj.evaltime.(LABEL) + toc(obj.timer.(LABEL));
        end
		function stop(obj,LABEL)
			obj.finish(LABEL);
        end
        
        % Get method for dependent properties
        function m = get.meantime(obj)
            labels = fields(obj.timer);
            mean = cellfun(@(l) obj.evaltime.(l)/obj.numcalls.(l), labels,'UniformOutput',false);
            m = cell2struct(mean,labels,1);
        end
    end
end