# NoN-SDP

NoN-SDP solves the following problem o minimisation an objective function $`f(X)`$ where $`X`$ is a column vector $`X\in\mathbb{R}^N`$ while a set of $`L`$ symmetric matrices $`\Delta_i`$ are positive definite and subjet to some linear equality constrains.
```math
\begin{array}{lll}
\text{Find:} & \min_{X\in\mathbb{R}^N} f(X)\\
    \text{Subject to:}&\Delta_i(X) \succeq 0 & 0<i<L\\
		&A\cdot X = B \end{array}
```
## Usage instructions
### Matrix function
A matrix function $`\Delta(X)`$ with domain $`\mathbb{R}^N`$ and image the set of $`M\times M`$ symmetric matrices is represented as a function:
```math
\Delta(X):\mathbb{R}^N \longrightarrow \mathbb{R}^{MxM}
```
The jacobian of the matrix function $`\Delta(X)`$ is a matrix of size $`M^2 \times N`$.

The second derivative of the matrix function $`\Delta(X)`$ is a matrix of size $`M^2 \times N^2`$.

Two different types of matrices $`\Delta_i`$ are distinguished:
+ **Non-Strictly feasible matrices:** matrices $`\Delta_{A_i}`$ that will be positive definite at the optimal solution to the problem but they may become not positive definite during the process of optimisation.
+ **Strictly feasible matrices:** matrices $`\Delta_{B_i}`$ that must remain positive definite all the time during the optimisation. These matrices are usefull, for instance, in a problem that is convex only for $`\Delta_i(x) \succeq 0`$, or in the case that the objective function $`f(X)`$ is undefined for $`\Delta_i(x) \succeq 0`$.
Note that the elements of the strictly feasible matrices must depend linearly on the vector $`X`$.


### Function handles
The problem implementation is based on three functions handles. A function handle that return the objective function $`f(X)`$ and two different functions handles to compute the matrices $`\Delta_{A_i}`$ and $`\Delta_{B_i}`$.

1. **Objective function:**   function handle with one input argument that return the objective function $`f(x)`$ in the first argument, the gradient of $`f(x)`$ in the second and the hessian matrix of  $`f(x)`$ in the third argument.
  ```matlab
  @(x)objectivefuntion(x)
  ```

2. **Strictly feasible matrices:**  function handle with one input argument that return the strictly feasible matrices  $`\Delta_{B_i}`$ as a column cell array in the first argument and the jacobian matrix of each  matrix $`\Delta_{B_i}`$ as a column cell array in the second argument.
```matlab
@(x)strictmatrix(x)
```
3. **Non-Strictly feasible matrices:** function handle with one input argument that return the non-strictly feasible matrices  $`\Delta_{A_i}`$ as well as the jacobian and the second derivative of each  matrix $`\Delta_{B_i}`$ as column cell arrays in the first, second, and third argument respectively.
```matlab
@(x)nonstrictmatrix(x)
```
Note that  the function handles are called with two input arguments when the second derivative is not required and with only one input argument when only the value of the objective function or the matrices is required. Therefore an implementation of the functions handles by checking the number of output arguments is strongly recomended:
```matlab
function [varargout] = objectivefunction(x,varargin)
% Computation of function value (func)
varargout{1} = func;
if gt(nargout,1)
    % Computation of the gradient (grad)
    varargout{2} = grad;
    if gt(nargout,2)
        % Computation of the hessian matrix (hess)
        varargout{3} = hess;
    end
end
end
```



## License


   NoN-SDP is distributed under GNU General Public License 3.0.


## Installation

To fully install NoN-SDP, perform the following steps:

1. Browse to the folder where you want the NoN-SDP code to be saved
2. Run the following git commands to clone the code to your computer

```
	git clone git@gitlab.inria.fr:MatchingToolbox/NoN-SDP.git
```

3. Finally, add NoN-SDP folder and its sub-folders to the matlab path

## Requirements

Matlab 2016b or higher. Most of the code has been tested on Matlab 2017b and 2018a
