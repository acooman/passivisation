% Options object for nonsdp
classdef nonopt
    properties 
        % Number of iterations
        max_outer_iter         (1,1) {mustBePositive,mustBeInteger} = 100;
        max_inner_iter         (1,1) {mustBePositive,mustBeInteger} = 50;
        max_ls_iter            (1,1) {mustBePositive,mustBeInteger} = 100;
        % Stop tolerance
        outer_tol              (1,1) {mustBePositive,mustBeReal} = 1e-3;
        % Min step
        min_step               (1,1) {mustBePositive,mustBeReal} = 1e-10;
        % Minimum function tolerance
        func_tol               (1,1) {mustBePositive,mustBeReal} = 1e-10;
        % Initialization
        initial_matrix_penalty (1,1) {mustBePositive,mustBeReal} = 1
        initial_linear_penalty (1,1) {mustBePositive,mustBeReal} = 1;
        initial_linear_mult    (1,1) {mustBePositive,mustBeReal} = 1;
        initial_matrix_mult    (1,1) {mustBePositive,mustBeReal} = 1;
        initial_matrix_barrier (1,1) {mustBePositive,mustBeReal} = 1;
        % Penalty_margin
        linear_penalty_margin  (1,1) {mustBePositive,mustBeReal} = 1.1;
        matrix_penalty_margin  (1,1) {mustBePositive,mustBeReal} = 1.1;
        % Barrier tolerance
        barrier_tolerance      (1,1) {mustBePositive,mustBeReal} = 1e-15;
        % Feasibility to stop
        min_matrix_feasibility (1,1) {mustBePositive,mustBeReal} = 1e-5;
        % Minimum penalty
        min_matrix_penalty     (1,1) {mustBePositive,mustBeReal} = 1e-9;
        min_linear_penalty     (1,1) {mustBePositive,mustBeReal} = 1e-5;
        max_linear_mult        (1,1) {mustBePositive,mustBeReal} = 1e5;
        % Minimum matrix barrier
        min_matrix_barrier     (1,1) {mustBePositive,mustBeReal} = 1e-10;
        % Maximum curvature in linsearch (relative to original curvature)
        max_curv               (1,1) {mustBePositive,mustBeLessThan(max_curv,1)} = 0.9;
        % Update parameters
        matrix_penalty_update  (1,1) {mustBePositive,mustBeLessThan(matrix_penalty_update,1)} = 0.5;
        matrix_barrier_update  (1,1) {mustBePositive,mustBeLessThan(matrix_barrier_update,1)} = 0.5;
        barrier_soft_update    (1,1) {mustBePositive,mustBeLessThan(barrier_soft_update,1)}   = 0.5;
        linear_penalty_update  (1,1) {mustBePositive,mustBeLessThan(linear_penalty_update,1)} = 0.5;
        linear_mult_update     (1,1) {mustBePositive,mustBeLessThan(linear_mult_update,1)}    = 0.5;
        matrix_mult_update     (1,1) {mustBeNonnegative,mustBeLessThan(matrix_mult_update,1)} = 0.5; 
        % Display
        display                (1,1) logical = true;
        % warm iterations
        warm_iter              (1,1) {mustBeNonnegative,mustBeInteger} = 0;
        % Check derivatives
        check_derivatives      (1,1) logical = false;
        % Expand matrix inequalities
        expand_inequalities    (1,1) logical = false;
        % check smallest eigenvalue of hessian matrix
        use_hessian            (1,1) logical = true;
    end
    
    methods
        function obj = nonopt(varargin)
            if ~isempty(varargin)
                obj = parseinputs(obj,varargin{:});
            end
        end
    end
end
