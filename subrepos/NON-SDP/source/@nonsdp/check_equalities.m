function chk = check_equalities(obj,eq)
% Verify the format and size of the equality coefficients matrices A and B
% Matrix A must be of size NEQ x Nx and B of size (Nx x 1) where NEQ is the
% number of equality constraints and Nx the number of variables.

if ~isfield(eq,'A') || ~isfield(eq,'B')
    error('Missing field A or B in equality constraints');
end
sza = size(eq.A);
szb = size(eq.B);
if ~(ismatrix(eq.A) && sza(2) == obj.Nx)
    error('A must be a 2D matrix with %d columns',obj.Nx);
end
if ~(isvector(eq.B) && all(szb==[sza(1),1]))
    error('B must be a vector of size %d x %d',sza(1),1);
end

if any(gt(abs(eq.A*obj.X0 - eq.B),1e-2))
    error('Initial point does not satisfy equalities');
end
obj.NE = sza(1);
chk = true;
end
