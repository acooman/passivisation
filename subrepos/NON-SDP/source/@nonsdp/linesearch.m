function [dir,F,lsiter,LSFLAG] = linesearch(FUN,X,dir0,F0,G0,options)
MAX_CURV = options.max_curv;
MAX_LSIT = options.max_ls_iter;
MIN_STEP = options.min_step;
FUNC_TOL = options.func_tol;

% initialise
lsiter = 0;
LSFLAG = 1;
    % Initial direction
    dir = dir0;
    
    % Curvature at initial point
    GA0 = G0 * dir0;
    
    if ge(GA0,0)
       dir = 0;
       F = F0;
       LSFLAG = -7;
       return; 
    end

    % Maximum line derivative
    CURV0 = MAX_CURV .* abs(GA0);
    
    % Curvature at sugested point
    [F,G] = FUN(X + dir0);
    GA = G*dir0; 

    
    % Find optimum step (find GA less than CURV0)
    d0 = 0;
    y0 = GA0;
    d1 = dir;
    y1 = GA;
    f1 = F;
    f0 = F0;
    
    while  ~isfinite(f1) || ge(abs(GA), CURV0)
        if isfinite(f1)
            %  Linear interpolation
            dir = d0 - y0 .* (d1-d0)./(y1-y0);
        else
            % Dichotomie here (halve interval)
            dir = (d1+d0)/2;
        end
        
        % Evaluate criterium
        [F,G] = FUN(X + dir);
        GA = G*dir0;
        
        if ge(norm(dir),norm(d1))
            d0 = d1;
            y0 = y1;
            f0 = f1;
            d1 = dir;
            y1 = GA;
            f1 = F;
        else
            if gt(GA,0) || ~isfinite(F)
                d1 = dir;
                y1 = GA;
                f1 = F;
            else
                d0 = dir;
                y0 = GA;
                f0 = F;
            end
        end
        lsiter = lsiter +1;
        
        if lt(norm(dir),MIN_STEP) || lt(norm(d1-d0),MIN_STEP)
            if lt(f1,f0)
                dir = d1;
                F = f1;
            else
                dir = d0;
                F = f0;
            end
           break;  
        end
        if lt(abs(f1-f0),FUNC_TOL)
            if lt(f1,f0)
                dir = d1;
                F = f1;
            else
                dir = d0;
                F = f0;
            end
           break;  
        end
        
        if lsiter == MAX_LSIT
            if lt(f1,f0)
                dir = d1;
                F = f1;
            else
                dir = d0;
                F = f0;
            end
%             LSFLAG = -8;
           break;
        end
    end
    if ~isfinite(F)
        LSFLAG = -6;
    end
end