function [X,F,EXITFLAG,ITER,LSIT] = newton_method(FUN,X,GRAD_OPT,options)
% EXITFLAG: 1 - Optimal solution within the given tolerances
%           2 - Minimum value of penalty/barrier
%           3 - Function change less than "func_tol"
%           4 - Current step smaller than minimum step size
%           5 - Max newton iterations reached
%           0 - Not solved
%          -1 - Max iterations reached
%          -2 - Max iterations reached. No feasible solution found
%          -3 - Objective functions diverges
%          -4 - Objective function is undefined at current point
%          -5 - Unconstrained optimisation failed
%          -6 - Linsearch failed
%          -7 - Function not decreasing in the choosen direction
%          -8 - Maximum number of linsearch iterations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
MAX_ITER = options.max_inner_iter;
MIN_STEP = options.min_step;
FUNC_TOL = options.func_tol;
USE_HESS = options.use_hessian;

% initialise
EXITFLAG = 5;
LSIT = 0;
for ITER = 1:MAX_ITER
    % Evaluate function
    if USE_HESS
        [F0,G0,H] = FUN(X);
    else
        [F0,G0] = FUN(X);
    end
    
    F = F0;
    if ~isfinite(F0)
        EXITFLAG = -4;
        break;
    end
    
    % Compute direction
    if norm(G0) < GRAD_OPT
        EXITFLAG = 1;
        break;
    else
        if USE_HESS
            [factor,pos]=chol(H);
            if pos==0
                dir0 = - factor \ (factor.' \ G0(:));
            else
                %                 disp(['Min eig = ',num2str(min(eig(H)))]);
                dir0 = -G0(:);
            end
        else
            dir0 = -G0(:);
        end
    end
    
    [dir,F,LSITER,LSFLAG] = nonsdp.linesearch(FUN,X,dir0,F0,G0,options);
    
    %% Check for error
    if LSFLAG < 0
        EXITFLAG = LSFLAG;
        return;
    end
    
    LSIT  = LSIT + LSITER;
    %% Update current point
    X = X + dir;
    
    if norm(dir) < MIN_STEP
        %% Step too small
        EXITFLAG = 4;
        break;
    end
    
    if abs(F-F0) < FUNC_TOL
        %% Change in objective too small
        EXITFLAG = 3;
        break;
    end
end

if ~isfinite(F)
    EXITFLAG = -5;
end
end
