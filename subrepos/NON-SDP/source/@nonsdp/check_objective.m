function check_objective(obj,objfun)
% Verify the objective function

[V{1:3}] = objfun(obj.X0);
% check objective
validateattributes(V{1},{'double'},{'scalar','finite'},'objfun','objective function');
validateattributes(V{2},{'double'},{'finite','row','ncols',obj.Nx},'objfun','objective gradient');
if ~isempty(V{3})
    validateattributes(V{3},{'double'},{'finite','square','size',[obj.Nx, obj.Nx]},'objfun','objective hessian');
end
obj.objfuncval = V;
end
