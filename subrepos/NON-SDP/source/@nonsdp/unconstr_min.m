function [X,FUNC,EXITFLAG,OUTPUT] = unconstr_min(FUN,X,options)
% unconstrained minimization
%
% EXITFLAG: 1 - Everything ok;
%           0 - Max newton steps
%          -2 - Minimum step size
%          -3 - Function change smaller than func_tol
%          -1 - Newton solver failed.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[X,FUNC,EXITFLAG,ITER,LSIT] = nonsdp.newton_method(FUN,X,options);

OUTPUT.NewtonCount = ITER;
OUTPUT.LineSearchIter = LSIT;
OUTPUT.LineSearchFlag  = 1;
OUTPUT.Message  = '';
end

