function [err_grad,err_hess,ERR_GRAD,ERR_HESS] = testDerivatives(obj)
x0 = obj.x;
numgradstep  = 1e-4;
errtol = 1e-5;

if ~isempty(obj.matfun(x0))
    validate(obj.matfun,x0,obj.GMAPA,errtol,numgradstep,true);
end
if ~isempty(obj.barfun(x0))
    validate(obj.barfun,x0,obj.GMAPB,errtol,numgradstep,false);
end
validate(@(X)objfun(X,obj.objfun),x0,[],errtol,numgradstep,true);


%% Validate barrier
% [~,GRAD,HESS] = nonsdp.matrix_barrier(obj.NBLIN,obj.PB,func,grad,hess);
% ERR_GRAD = abs(GRAD-NGRAD).^2;
% ERR_GRAD = max(ERR_GRAD(:));
% ERR_HESS = abs(HESS-NHESS).^2;
% ERR_HESS = max(ERR_HESS(:));
% if (ERR_GRAD > 1e-3)
%     error('Barrier Gradient verification failed. Max error = %10.4e ',ERR_GRAD);
% end
% if (ERR_HESS > 1e-3)
%     error('Barrier Hessian verification failed. Max error = %10.4e ',ERR_HESS);
% end


end

function validate(fun,x0,GMAP,errtol,numgradstep,validatehess)

if validatehess
    [~,grad,hess] = fun(x0);
else
    [~,grad] = fun(x0);
end

% when GMAPs are used, the returned gradient and hessian are padded with zeros to give them the right size
if ~isempty(GMAP)
    if ~all(cellfun(@all,GMAP))
        for ff=1:length(grad)
            T = grad{ff};
            grad{ff} = zeros(size(T,1),length(x0));
            grad{ff}(:,GMAP{ff})=T;
        end
        % TODO: the hessian ...
    end
end

if validatehess
    [numgrad,numhess] = numericDerivatives(fun,x0,numgradstep,size(grad,1));
else
    numgrad = numericDerivatives(fun,x0,numgradstep,size(grad,1));
end

%% Validate gradient
err_grad = cellfun(@(x,y) max(abs(x(:)-y(:)).^2),grad,numgrad);
err_grad = max(err_grad);
if any(err_grad > errtol)
    for nn=1:length(grad)
        figure;
        subplot(131);
        spy(grad{nn})
        subplot(132)
        spy(numgrad{nn})
        subplot(133)
        spy(abs((grad{nn}-numgrad{nn}))>1e-3,'r')
    end
    error('Gradient verification failed. Max error = %10.4e ',max(err_grad));
end
%% Validate hessian
if validatehess
    for i = 1:length(hess)
        if isempty(hess{i})
            hess{i} = 0;
        end
    end
    err_hess = cellfun(@(x,y) max(abs(x(:)-y(:)).^2),hess,numhess);
    err_hess = max(err_hess);
    if any(err_hess > errtol)
        error('Hessian verification failed. Max error = %10.4e ',max(err_hess));
    end
end
end

function [numgrad,numhess] = numericDerivatives(func,X,h,N)
NX = length(X);
numgrad = cell(N,1);
if nargout>1
    numhess = cell(N,1);
end
% for each dimension of objective function
for i=1:NX
    % vary variable i by a small amount (left and right)
    x1 = X;
    x2 = X;
    x1(i) = X(i) - h;
    x2(i) = X(i) + h;
    
    % evaluate the objective function at the left and right points
    [y1,g1] = func(x1);
    [y2,g2] = func(x2);
    
    % calculate the slope (rise/run) for dimension i
    grad = cellfun(@(Y1,Y2) (Y2(:)-Y1(:))./(2*h),y1,y2,'UniformOutput',false);
    numgrad = cellfun(@(a,b) [a,b],numgrad,grad,'UniformOutput',false);
    
    if nargout>1
        hess = cellfun(@(G1,G2) (G2-G1)./(2*h),g1,g2,'UniformOutput',false);
        numhess = cellfun(@(a,b) cat(3,a,b),numhess,hess,'UniformOutput',false);    
    end
end
if nargout>1
    numhess = cellfun(@(a) reshape(a,size(a,1),[]),numhess,'UniformOutput',false);
end
end


function [fun,grad,hess]=objfun(X,fun)
    [fun,grad,hess]=fun(X);
    fun={fun};
    grad = {grad};
    hess = {hess};
end





