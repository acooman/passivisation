function updateMatrixPenalty(obj)
% Penalty update
pmin = obj.options.min_matrix_penalty;
marg = obj.options.matrix_penalty_margin;
A = obj.matval{1};
PA = obj.options.matrix_penalty_update .* obj.PA;
obj.PA = obj.checkMatrixPenalty(A,max(pmin,PA),obj.PA,marg);
end
