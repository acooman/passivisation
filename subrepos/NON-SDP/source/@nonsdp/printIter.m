function printIter(obj,iter,NewtonCount,LineSearchIter,FVAL,innertol)
if obj.options.display
    % Header labels
    labels = {...
        'Iter',...
        'Newt',...
        'LsIt',...
        'Objfun f(x)',...
        '|f(x)-F(x)|'};
    
    % Compute arguments
    args = {iter,NewtonCount,LineSearchIter,...
            obj.objfuncval{1},abs(obj.objfuncval{1}-FVAL)};
    format = '%5d%5d%5d%13.4e%13.4e';
    header = '%5s%5s%5s%13s%13s';

%     % Add innertolerance
%     labels = [labels,'innToleranc'];
%     args = [args,innertol];
%     format = [format,'%13.4e' ];
%     header = [header,'%13s'];

    if obj.NA 
        args   = [args,obj.mat_feas,min(obj.PA)];
        labels = [labels,'Min mat eig','Min P mult'];
        format = [format,'%13.4e%13.4e'];
        header = [header,'%13s%13s'];
    end
    if obj.NB 
        args   = [args,obj.bar_feas,min(obj.PB)];
        labels = [labels,'Min fsb eig','Min P bar'];
        format = [format,'%13.4e%13.4e'];
        header = [header,'%13s%13s'];
    end
    if obj.NC
        args   = [args,obj.unfbar_feas,min(obj.PC), min(obj.PC2)];
        labels = [labels,'Min unf eig','Min P unf', 'Unf. mult.'];
        format = [format,'%13.4e%13.4e%13.4e'];
        header = [header,'%13s%13s%13s'];
    end
      
    % Add exit flag    
    labels = [labels, 'Info'];
    if iter == 0
        args   = [args,'Initial Point'];
    else
        args   = [args,obj.OUTMSG];
    end
    format = [format,'   %-13s\n'];
    header = [header,'%7s\n'];
    
    % Printing
    if mod(iter,20) == 0
        fprintf(header,labels{:});
    end
    fprintf(format,args{:});
end
end




