function updateMatrixMultipliers(obj)

% Matrix Multipliers
% Try a fix lambda so Uk+1 is not too different from Uk


l  = obj.options.matrix_mult_update;
A  = obj.matval{1};
P  = obj.PA;
U  = obj.UA;

if iscell(A)
    for i=1:length(A)
        n  = size(A{i},1);
        Id = speye(n);
        Z  = (P*Id + A{i})\Id;
        UA = P^2 * Z * U{i} * Z;
        l  = min(l,l*(normf(U{i}))/(normf(UA-U{i})));
        obj.UA{i} = l * UA + (1-l) * U{i};
    end
    
else
    n  = size(A,1);
    Id = speye(n);
    Z  = (P*Id + A)\Id;
    UA = P ^2 * Z * U * Z;
    l  = min(l,l*(normf(U))/(normf(UA-U)));
    obj.UA = l * UA + (1-l) * U;
end

end

