function updateLinearPenalty(obj)

updt = obj.options.linear_penalty_update;
updm = obj.options.linear_mult_update;
% Penalty update
pmin = obj.options.min_linear_penalty;
pmax = obj.options.max_linear_mult;

    PC =  updt .* obj.PC;
    % Check penalty
    marg = obj.options.linear_penalty_margin;
    A = obj.unfbarval;
    newPC = obj.checkLinearPenalty(A,max(pmin,PC),marg);
    
    % Check feas
    feas = le( -obj.unfbar_feas,pmin);
    
    
    % Update multipliers
    ind = gt(newPC,PC);
    if feas
        obj.PC2 = max(1,obj.PC2 * updm);
    else
    obj.PC2(ind) = min(pmax, obj.PC2(ind)./updm);
    end
    ind = ge(newPC,obj.PC);
    newPC(ind) = obj.PC(ind);
    obj.PC = newPC;
    
end