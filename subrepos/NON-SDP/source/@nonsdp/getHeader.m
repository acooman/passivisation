% Redefine header for custom display
function header = getHeader(obj)
if ~isscalar(obj)
    header = getHeader@matlab.mixin.CustomDisplay(obj);
else
    headerStr = matlab.mixin.CustomDisplay.getClassNameForHeader(obj);
    header = sprintf('%s %s\n',headerStr,'problem object v1');
end
end
