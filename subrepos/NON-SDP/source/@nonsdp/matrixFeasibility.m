function feas = matrixFeasibility(A)
feas = cellfun(@update_feas,A);
feas = min(feas);
end


function feas = update_feas(A)
feas = min(real(eig(A)));
end