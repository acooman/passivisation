function chk = check_derivatives(obj)

x0          = obj.x;
numgradstep = 1e-4;
errtol      = 1e-3;

% Validate function handles
validate(@(x)obj.objfun(x),x0,errtol,numgradstep,'objfun');
if obj.NA
    validate(@(x)get_matrix(x,obj.matfun,obj.GMAPA,obj.penalty_change),x0,errtol,numgradstep,'matfun');
end
% Validate linear matrices
if obj.NB
    validate(@(x)get_linearmatrix(x,obj.matfeasible,obj.GMAPB,obj.Bct),x0,errtol,numgradstep,'matfeasible');
end
if obj.NC
    validate(@(x)get_linearmatrix(x,obj.matunfeasible,obj.GMAPA,obj.Cct),x0,errtol,numgradstep,'matunfeasible');
end
end


function validate(fun,x0,tol,numgradstep,funname)
% Numeric derivatives
[grad_num,hess_num] = numeric_derivative(fun, x0, numgradstep);
% Exact derivatives
[~,grad,hess] = fun(x0);
% Validate gradient
ind = isfinite(grad(:)) & isfinite(grad_num(:));
err_grad = max(abs(grad_num(ind)-grad(ind)));
if err_grad > tol 
        figure;
        subplot(131);
        spy(grad)
        title('gradient')
        subplot(132)
        spy(grad_num)
        title('numeric gradient')
        subplot(133)
        spy(abs((grad-grad_num))>tol,'r')
        title('abs(grad-numgrad)>tol')
    error('Gradient verification of %s failed. Max error = %10.4e ',funname,max(err_grad));
end
% Validate hessian
ind = isfinite(hess(:)) & isfinite(hess_num(:));
err_hess = max(abs(hess_num(ind)-hess(ind)));
if err_hess > tol 
        figure;
        subplot(131);
        spy(hess)
        subplot(132)
        spy(hess_num)
        subplot(133)
        spy(abs((hess-hess_num))>tol,'r')
        error('Hessian verification of %s failed. Max error = %10.4e ',funname,max(err_hess));
end

end

function [varargout] = get_linearmatrix(X,GRAD,GMAP,CNT)
% Construct matrices
varargout{1} = cellfun(@(M,I,ct) M*X(I) + ct,GRAD,GMAP(:),CNT,'UniformOutput',false);
% Construct gradient
if nargout > 1
    % Nonzero indexes
    [I,J] = cellfun(@(A,G) find(repmat(reshape(G,1,[]),[numel(A),1])),varargout{1},GMAP(:),'UniformOutput',false);
    % Number of variables
    n = length(X);
    % Add zeros by creating sparse matrix
    varargout{2} = cellfun(@(I,J,V) sparse(I(:),J(:),V(:),size(V,1),n) ,I,J,varargout{2},'UniformOutput',false);
    if nargout > 2
        % hessian matrix (very easy)
        varargout{3} = cellfun(@(A) sparse(size(A,1),n^2) ,varargout{1},'UniformOutput',false);
    end
end
% Concatenate all matrices
varargout = cellfun(@(x) cat(1,x{:}),varargout,'UniformOutput',false);
end

function [varargout] = get_matrix(X,FUN,GMAP,VARCHANGE)
[varargout{1:nargout}] = FUN(X);
% Reshape output arguments
varargout{1} = cellfun(@(x) x(:),varargout{1},'UniformOutput',false);

if nargout > 1
    % Change of variable if required
    for ii=1:length(VARCHANGE)
        varargout{2}{ii} =  varargout{2}{ii} * VARCHANGE{ii};
    end
    % Nonzero indexes
    [I,J] = cellfun(@(A,G) find(repmat(reshape(G,1,[]),[numel(A),1])),varargout{1},GMAP(:),'UniformOutput',false);
    % Number of variables
    n = length(X);
    % Add zeros by creating sparse matrix
    varargout{2} = cellfun(@(I,J,V) sparse(I(:),J(:),V(:),size(V,1),n) ,I,J,varargout{2},'UniformOutput',false);
    
    % do the same with the hessian
    if nargout > 2
        for ii=1:length(VARCHANGE)
            % Number of non zero elements in hessian
            nz = sqrt(size(varargout{3}{ii},2));
            % Number of non-zero elements after variable change
            mz = size(VARCHANGE{ii},2);
            % reshape hessian
            varargout{3}{ii} = reshape(varargout{3}{ii}.',nz,nz,[]);
            % Change of variable
            varargout{3}{ii} = reshape(VARCHANGE{ii}.' * varargout{3}{ii}(:,:),mz,nz,[]);
            varargout{3}{ii} = reshape(permute(varargout{3}{ii}, [1 3 2]), [],nz);
            varargout{3}{ii} = reshape(permute(reshape(varargout{3}{ii} * VARCHANGE{ii}, mz, [], mz), [2,1,3]),[],mz^2);
        end
        % Add zeros in empty hessian entries
        for ii=1:length(varargout{3})
            if isempty(varargout{3}{ii})
                varargout{3}{ii} = sparse(size(varargout{2}{ii},1),sum(GMAP{ii})^2);
            end
        end
        % Nonzero indexes in hessian
        [I,J] = cellfun(@(A,G) find(repmat(reshape(G(:)&reshape(G,1,[]),1,[]),[numel(A),1])),varargout{1},GMAP(:),'UniformOutput',false);
        % Add zeros by creating sparse matrix
        varargout{3} = cellfun(@(I,J,V) sparse(I(:),J(:),V(:),size(V,1),n^2) ,I,J,varargout{3},'UniformOutput',false);
    end
end

% Concatenate all matrices
varargout = cellfun(@(x) cat(1,x{:}),varargout,'UniformOutput',false);
end

function [D,H] = numeric_derivative(func, X, h)
% Evaluate objective function
F = func(X);
% Initialise gradient and hessian
D = zeros(numel(F),numel(X));
if nargout ==2
    H = zeros(numel(F),numel(X),numel(X));
end

% for each dimension of objective function
for i=1:numel(X)
    % vary variable i by a small amount (left and right)
    x1 = X;
    x2 = X;
    x1(i) = X(i) - h;
    x2(i) = X(i) + h;
    
    % evaluate the objective function at the left and right points
    [F1{1:nargout}] = func(x1);
    [F2{1:nargout}] = func(x2);
    
    % calculate the slope (rise/run) for dimension i
    D(:,i) = (F2{1}(:) - F1{1}(:)) / (2*h);
    
    % Numeric hessian
    if nargout == 2
        H(:,:,i) = (F2{2} - F1{2}) / (2*h);
    end
end
if nargout ==2
    H = H(:,:);
end
end