function expand_inequalities(obj)

% Expand complex matrices
for ii=1:obj.NC
   if  ~isreal(obj.C0{ii}) || ~isreal(obj.matunfeasible{ii})
       S = size(obj.C0{ii},1);
       MR = real(obj.matunfeasible{ii});
       MI = imag(obj.matunfeasible{ii});
       M1 = [reshape(MR,S,[]); reshape(MI,S,[])];
       M2 = [reshape(-MI,S,[]); reshape(MR,S,[])];
       obj.matunfeasible{ii} = [reshape(M1,2*S^2,[]);reshape(M2,2*S^2,[])];
       obj.C0{ii} = [real(obj.C0{ii}), -imag(obj.C0{ii}); imag(obj.C0{ii}), real(obj.C0{ii}) ];
   end
end



% Preallocate equalities
N  = cellfun('prodofsize',obj.C0);
EQ = zeros(obj.NE+sum(N),obj.Nx + sum(N));
X0 = obj.X0;

% User equalities
if obj.NE
    EQ(1:obj.NE,1:obj.Nx) = obj.LEQ.A;
end


n = obj.NE;
m = obj.Nx;
% Recompute numbre of equalities and variables
obj.Nx = obj.Nx + sum(N);
obj.NE = obj.NE + sum(N);



% Expanded equalities
    ind = reshape(1:obj.Nx,[],1);
for ii=1:obj.NC
    EQ(n+1:n+N(ii),ind(obj.GMAPC{ii})) = obj.matunfeasible{ii};
    EQ(n+1:n+N(ii),m+1:m+N(ii)) = -eye(N(ii));
    % Recompute gradient and parameters
    obj.matunfeasible{ii} = eye(N(ii));
    obj.GMAPC{ii} = false(1,obj.Nx);
    obj.GMAPC{ii}(1,m+1:m+N(ii)) = true;
    X0 = [X0; obj.C0{ii}(:)];
    n = n+N(ii);
    m = m+N(ii);
end

obj.LEQ.A = EQ;
obj.X0 = X0;

end