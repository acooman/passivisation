function updateMatrixBarrier(obj)
% Barrier parameter
pmin = obj.options.min_matrix_barrier;
if lt(obj.PB,0.1)
    PB = obj.PB * obj.options.barrier_soft_update;
else
    PB   = obj.options.matrix_barrier_update * obj.PB;
end
obj.PB = max(pmin,PB);
end
