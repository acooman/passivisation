function [FLAG,FUNC,GRAD,HESS] = penalty(P,U,Y2X,A,DA,D2A)
% Evaluate the penalty/barrier function for the matrix A with the penalty
% weigh P. See [1].
% The penalty and its derivatives are obtained as:
%
%       f   = P^2 * Z - P*I
%      dfdx =-P^2 * Z * DAi * Z
% d2fdxidxj = P^2 * Z *(DAi * Z * dAj - D2Aij + dAj * Z * DAi) * Z
%
% with Z = (P*I + A)^-1
%
% The furnction return <U,f> = trace(U * f)

%% init flag
FLAG = 0;

%% Compute penalty
na = size(A,1);
Id = speye(na);
% Check matrix is positive definite
[z,pos] = chol(P .* Id + A);
if ~all(isfinite(A(:))) || pos~=0
    FLAG = -1;
    FUNC = inf;
    GRAD = inf;
    HESS = inf;
    return;
end
Z  = z\(z'\Id);
F  = P^2 * Z - P * Id;
U  = full(U);
UF = U.*F;
FUNC = (sum(UF(:)));

%% Compute gradient
if nargout > 2
    Ng  = size(DA,2);
    if issparse(DA)
        DA = full(DA);
    end
    DA  = reshape(DA,na,na,[]);
    % Product Z * DA * Z
    ZDA = reshape(Z*DA(:,:),[na na Ng]);
    ZDA = reshape(permute(ZDA, [1 3 2]), [na*Ng, na]);
    ZAZ = permute(reshape(ZDA*Z, [na Ng na]), [1 3 2]);
    G = -P^2 .* ZAZ;
    UG  = U.*G;
    GRAD = (sum(reshape(UG,na^2,Ng),1));
    if ~isempty(Y2X)
       GRAD = GRAD * Y2X; 
    end
    
    %% Compute Hessian
    if nargout > 3
        % Index j & i in the lower triangle
        lw = tril(true(Ng));
        I = repmat(1:Ng,Ng,1);
        J = I.';
        I = I(lw);
        J = J(lw);
        % Number of elements in lower triangle
        N  = (Ng^2 + Ng)/2;
        
        % Introduce singleton in third dimension (for matrix product)
        DA  = reshape(DA,na,na,1,Ng);
        % Introduc singletion in first dimension (to multiply by DA)
        ZAZ = reshape(ZAZ,1,na,na,Ng);

        % Elements of D2A and ZAZ in lower triangle
        DAi = DA(:,:,:,I);
        DAj = DA(:,:,:,J);
        ZAZi = ZAZ(:,:,:,I);
        ZAZj = ZAZ(:,:,:,J);
        
        % Matrix product
        M = sum(DAi .* ZAZj + DAj .* ZAZi,2);
        M = reshape(M,na,na,N);
        
        if ~isempty(D2A)
            D2A  = reshape(D2A(:,lw),na,na,[]);
            D2A  = reshape(permute(D2A, [1 3 2]), [na*N na]);
            D2AZ = permute(reshape(D2A*Z, [na N na]), [1 3 2]);
            M = M - D2AZ;
        end
        UH = U.* P^2 .* reshape(Z*M(:,:),[na na N]);
        Hij = sum(reshape(UH,na^2,N),1);
        
        % Preallocate hessian matrix
        H     = zeros(Ng);
        % Fill lower triangle with elements in Ahess
        H(lw) = (Hij);
        HESS  = H + tril(H,-1).';
        if ~isempty(Y2X)
           HESS = Y2X.' * HESS * Y2X; 
        end
    end
end
end
