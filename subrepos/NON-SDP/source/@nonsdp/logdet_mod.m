% Test barrier derivativefun
function [FLAG,F,G,H] = logdet_mod(mu,A,DA,DAt)
% Initialise flag
FLAG = 0;
% Make sure A is hermitian
A = (A+A')/2;
I = eye(size(A));
% Cholesky factorization
[T,pos]=chol(A./mu + I);
if pos~=0
    FLAG = -1;
    F = inf;     G = inf;     H = inf;
    return;
end
% log of determinant
F = - real(mu*2*sum(log(diag(T))));
% derivative
if nargout >2
    Ai = conj(T\(T'\I));
    G  = - real(reshape(Ai,1,[]) * DA);
    % Hessian
    if nargout >3
        Hp = kron(Ai,conj(Ai));
        H  = (1./mu) .* real(DAt * Hp * DA);
%         n  = size(DA,2);
%         H  = (1./mu) .* real(reshape(DAt * Hp(:),n,n));
    end
end