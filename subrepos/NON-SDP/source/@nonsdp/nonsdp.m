classdef nonsdp < handle & matlab.mixin.CustomDisplay
    %    NoN-SDP is a fork of the  open source software PENLAB for nonlinear
    %    optimization, linear and nonlinear semidefinite optimization and any
    %    combination of these. It has been rewriten from scratch fixing some
    %    issues and adding some simplifications.
    
    properties (SetAccess = private)
        % Problem name. Label only used in the display
        name;
        % Number of variables
        Nx = 0;
        % Number of non-linear inequalities
        NA = 0;
        % Number of strict feasible linear matrix inequalities
        NB = 0;
        % Number of not-feasible linear matrix inequalities
        NC = 0;
        % Number of linear equality constraints
        NE = 0;
        % Output flags
        OUTFLAG = 0;
        % Size of matrices
        SA = [];
        SB = [];
        SC = [];
    end
    properties (Dependent)
        % Current point
        x;
        % Feasible linear matrices
        barval;
        % Unfeasible linear matrices
        unfbarval;
        % output message
        OUTMSG;
    end
    properties (SetAccess = private)
        % Map of matrix dependences
        GMAPO = [];
        GMAPA = [];
        GMAPB = [];
        GMAPC = [];
        % Callbacks functions
        objfun@function_handle scalar;
        matfun@function_handle scalar;
        % Linear objective
        objlingrad = [];
        % Linear matrices gradient
        matfeasible = [];
        matunfeasible = [];
        matunfeasible_t = [];

        % Initial matrices A and B
        B0;
        C0;
        % Linear matrix constant
        Bct;
        Cct;
        % Linear Equality constraints
        LEQ;
        % Kernel of equality matrix
        KA;
        % Last value of function evaluations
        objfuncval@cell matrix;
        barrpenval@cell matrix;
        matval@cell matrix;
        % Matrix feasibility
        mat_feas = Inf;
        bar_feas = Inf;
        unfbar_feas = Inf;
        % Initial point
        X0 = [];
        % Current point
        W = [];
        % Variable change for non-linear matrix gradient if required
        penalty_change;
        %% Lagrange multipliers and penalties
        % Matrix Penalty
        PA = 0;
        PB = 0;
        PC = 0;
        PC2 = 1;
        % Matrix Lagrange Multipliers
        UA;
        %% Other information
        % Norm of gradient of augmented lagrangian
        norm_grad;
        % Option object
        options;
        % Statistics object
        stats = TimeStats;
        % Linear objective
        linobj = false;
        % Keep moving barrier
        movebarrier = true;
    end
    methods
        function obj = nonsdp(varargin)
            % Class constructor
            
            % INPUT PARSER FOR BASIC INFORMATION
            p=inputParser;
            p.FunctionName = 'NoNSDP';
            p.StructExpand = true;
            p.KeepUnmatched = true;
            defname = 'NoNSDP NL-SDP Problem';
            % Problem name
            p.addParameter('name',     defname,    @ischar);
            % Map of matrix dependence
            p.addParameter('GMAPA',  [],  @(x) validateattributes(x,{'cell'},{'row'}));
            p.addParameter('GMAPB',  [],  @(x) validateattributes(x,{'cell'},{'row'}));
            p.addParameter('GMAPC',  [],  @(x) validateattributes(x,{'cell'},{'row'}));
            % Inital point. It must be a feasible point
            p.addParameter('X0',     [],  @(x) check_initialpoint(obj,x));
            p.addParameter('B0',     [],  @(x) true);
            p.addParameter('C0',     [],  @(x) true);

            % PARSE INPUTS
            p.parse(varargin{:});
            fields = fieldnames(p.Results);
            for ff=1:length(fields) %#ok<FORFLG>
                obj.(fields{ff}) = p.Results.(fields{ff});
            end
            p2=inputParser;
            p2.FunctionName = 'NoNSDP';
            p2.StructExpand = true;
            % Callback functions
            p2.addParameter('objfun', @(x)[],  @(x) check_objective(obj,x));
            p2.addParameter('matfun', @(x)[],  @(x) check_matpenalty(obj,x));
            p2.addParameter('matfeasible',@(x)[],  @(x) check_matbarrier(obj,x));
            p2.addParameter('matunfeasible',@(x)[],  @(x) check_unfbarrier(obj,x));
            p2.addParameter('penalty_change', {},  @iscell);
            % Linear Equality constraints
            % Structure containing the fields LEQ.A and LEQ.B such that:
            %
            %               LEQ.A * X = LEQ.B
            %
            % Matrix A must be of size NEQ x Nx and B of size (Nx x 1) where NEQ is the
            % number of equality constraints and Nx the number of variables.
            p2.addParameter('LEQ',    [],  @(x) check_equalities(obj,x));
            % Problem options. Nonopt object containing all the options settings
            p2.addParameter('options',   nonopt,    @(x)isa(x,'nonopt'));
            
            % PARSE INPUTS
            p2.parse(p.Unmatched);
            fields = fieldnames(p2.Results);
            for ff=1:length(fields) %#ok<FORFLG>
                obj.(fields{ff}) = p2.Results.(fields{ff});
            end
            
            %Initialize Problem
            obj.initialise;
        end
        %% SET AND GET METHODS
        function MSG = get.OUTMSG(obj)
           switch obj.OUTFLAG 
               case 1
                   MSG = 'Norm of gradient less than "inner_tol"';
               case 2
                   MSG = 'Minimum value of penalty/barrier';
               case 3
                   MSG = 'Function change less than "func_tol"';
               case 4
                   MSG = 'Current step smaller than minimum step size';
               case 5
                   MSG = 'Max newton iterations reached';
               case 0
                   MSG = 'Not solved';
               case -1
                   MSG = 'Max iterations reached';
               case -2
                   MSG = 'Max iterations reached. No feasible solution found';
               case -3
                   MSG = 'Objective functions diverges';
               case -4
                   MSG = 'Objective function is undefined at current point';
               case -5
                   MSG = 'Unconstrained optimisation failed';
               case -6
                   MSG = 'Linsearch failed';
               case -7
                   MSG = 'Function not decreasing in the choosen direction';
               case -8
                   MSG = 'Maximum number of linsearch iterations';
               otherwise
                   MSG = 'Unknown stopping flag';
           end
        end
        function x = get.x(obj)
            if obj.NE
                x = obj.X0 + obj.KA * obj.W;
            else
                x = obj.W;
            end
        end
        function B = get.barval(obj)
            B = cellfun(@(M,I,ct)reshape(M * obj.x(I) + ct,sqrt(size(M,1)),sqrt(size(M,1))),obj.matfeasible,obj.GMAPB(:),obj.Bct,'UniformOutput',false);
        end
        function C = get.unfbarval(obj)
            C = cellfun(@(M,I,ct)reshape(M * obj.x(I) + ct,sqrt(size(M,1)),sqrt(size(M,1))),obj.matunfeasible,obj.GMAPC(:),obj.Cct,'UniformOutput',false);
        end
    end
    methods (Access=private)
        %% OTHER METHODS
        varargout = check_initialpoint(varargin)
        varargout = check_objective(varargin)
        varargout = check_matpenalty(varargin)
        varargout = check_matbarrier(varargin)
        varargout = check_unfbarrier(varargin)
        varargout = check_equalities(varargin)
        varargout = testDerivatives(varargin)
        varargout = augmented_lagrangian(varargin)
        varargout = initialise(varargin)
        varargout = updateMatrixMultipliers(varargin)
        varargout = updateLinearMultipliers(varargin)
        varargout = updateMatrixPenalty(varargin)
        varargout = updateLinearPenalty(varargin)
        varargout = printIter(varargin)
    end
    methods (Access=protected)
        header  = getHeader(obj)
        propgrp = getPropertyGroups(obj)
    end
    methods (Static,Access=private)
        varargout = logdet(varargin)
        varargout = logdet_mod(varargin)
        varargout = logdet_mod_expanded_matrix(varargin)
        varargout = penalty(varargin)
        varargout = checkMatrixPenalty(varargin)
        varargout = checkLinearPenalty(varargin)
        varargout = newton_method(varargin)
        varargout = linesearch(varargin)
        varargout = matrixFeasibility(varargin)
    end
end

