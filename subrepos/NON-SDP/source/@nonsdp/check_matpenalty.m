function check_matpenalty(obj,matfun)
% Verify the objective function

[V{1:3}] = matfun(obj.X0);
obj.matval = V;
V = [V{:}];
% Check penalties matrix
NA = size(V,1);
for ii=1:NA
     check_matrix(V{ii,:});    
end
obj.NA = NA;
end

function check_matrix(mat,grad,hess)
    validateattributes(mat,{'double'},{'finite','square'},'matfun','matrix constraint');
    SA = numel(mat);
    validateattributes(grad,{'double'},{'finite','2d','size',[SA, NaN]},'matfun','derivative of matrix');
    if ~isempty(hess)
        validateattributes(hess,{'double'},{'finite','2d','size',[SA, NaN]},'matfun','second derivative of matrix');
    end
end
