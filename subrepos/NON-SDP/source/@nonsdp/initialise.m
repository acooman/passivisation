function initialise(obj)

if obj.options.display
    fprintf(['Initialising ',obj.name,' ... ']);
end

% Initialise statistics
obj.stats.init;

% Time initialise function
obj.stats.start('init');

if obj.options.expand_inequalities
    obj.expand_inequalities;
end

% Check if criterium is linear
if isempty(obj.objfuncval{3})
    obj.linobj = true;
    obj.GMAPO = logical(obj.objfuncval{2});
    obj.objlingrad = obj.objfuncval{2}(obj.GMAPO);
    obj.objfuncval{3} = sparse(obj.Nx,obj.Nx);
else
    obj.GMAPO = true(1,obj.Nx);
end

if obj.NE
    % Kernel of A
    obj.KA = null(obj.LEQ.A);
    % Initialize W
    obj.W = zeros(size(obj.KA,2),1);
else
    obj.W = obj.X0;
end

if obj.NA
    % Matrix multipliers and penalty
    immult = obj.options.initial_matrix_mult;
    impen  = obj.options.initial_matrix_penalty;
    pmin = obj.options.min_matrix_penalty;
    marg = obj.options.matrix_penalty_margin;
    obj.UA = cellfun(@(x) immult * speye(size(x)),obj.matval{1},'UniformOutput',false);
    obj.PA = obj.checkMatrixPenalty(obj.matval{1},max(pmin,impen),inf,marg);
    
    % size of matrices
    obj.SA = cellfun('size',obj.matval{1},1);
end

if obj.NB
    % Compute constant term
    obj.Bct = cellfun(@(B0,M,ii) B0(:) - M * obj.X0(ii),obj.B0,obj.matfeasible,obj.GMAPB(:),'UniformOutput',false);
    % Compute size of matrices
    obj.SB = sqrt(cellfun('size',obj.matfeasible,1));
    % Strict feasible matrix penalty
    obj.PB = obj.options.initial_matrix_barrier;
end

if obj.NC
    % Compute constant term
    obj.Cct = cellfun(@(C0,M,ii) C0(:) - M * obj.X0(ii),obj.C0,obj.matunfeasible,obj.GMAPC(:),'UniformOutput',false);
    % Compute hermitian matrix to save some time later
%     obj.matunfeasible_t = cellfun(@(x) kron(x.',x'),obj.matunfeasible,'UniformOutput',false);
    obj.matunfeasible_t = cellfun(@(x) x',obj.matunfeasible,'UniformOutput',false);

    % size of matrices
    obj.SC = sqrt(cellfun('size',obj.matunfeasible,1));
    % Strict feasible matrix penalty
    marg = obj.options.linear_penalty_margin;
    ipc    = repmat(obj.options.min_linear_penalty,[obj.NC,1]);
    obj.PC = obj.checkLinearPenalty(obj.unfbarval,ipc,marg);
    if all(lt(obj.PC, obj.options.initial_linear_penalty))
        obj.PC2 = 1./obj.PC;
    else
        obj.PC2 = repmat(obj.options.initial_linear_mult,[obj.NC,1]);
    end
end

obj.stats.finish('init');
if obj.options.display
    fprintf('Done \n');
end
end
