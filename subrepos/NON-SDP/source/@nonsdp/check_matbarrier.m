function check_matbarrier(obj,matfeasible)
% Check barrier matrix
NB = length(matfeasible);
obj.NB = NB;
end

function check_matrix(mat,grad)
    validateattributes(mat,{'double'},{'finite','square'},'matbar','barriered matrix');
    SB = numel(mat);
    validateattributes(grad,{'double'},{'finite','2d','size',[SB, NaN]},'matbar','derivative of barriered matrix');
end
