function solve(obj)
obj.stats.start('solve');
FUN   = @(x) obj.augmented_lagrangian(x);
% Minimum penalty weight
minPA = obj.options.min_matrix_penalty;
minPB = obj.options.min_matrix_barrier;
minPC = obj.options.min_linear_penalty;

% Matrix tolerance
eps_mfeas  = obj.options.min_matrix_feasibility;
% Initilialise tolerance to stop inner iteration
inner_tol  = 1e0 * obj.options.outer_tol;
% Initialise flags
obj.OUTFLAG  = -1;
FEASIBLE   = false;
CONVERGED  = false;
MINWEIGHT  = false;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% OUTERFLAG:1 - Optimal solution within the given tolerances
%           3 - Function change less than "func_tol"
%           4 - Current step smaller than minimum step size
%           5 - Max newton iterations reached
%           0 - Not solved
%          -1 - Max iterations reached
%          -2 - Max iterations reached. No feasible solution found
%          -3 - Objective functions diverges
%          -4 - Objective function is undefined at current point
%          -5 - Unconstrained optimisation failed
%          -6 - Linsearch failed
%          -7 - Function not decreasing in the choosen direction
%          -8 - Maximum number of linsearch iterations
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Print Header
if obj.options.display
    printHeader(obj.name,obj.Nx,obj.NA,obj.NB,obj.NC,obj.NE)
end


% Print starting info
if obj.options.display
    % Check feasibility
    check_feas(obj);
    FVAL = FUN(obj.W);
    obj.printIter(0,0,0,FVAL,0);
end


for it = 1:obj.options.max_outer_iter
    
    % Old criterium
    fval_0      = obj.objfuncval{1};
    
    % Unconstraint minimization
    obj.stats.start('inner_iter');
    [obj.W,FVAL,obj.OUTFLAG,NewtonCount,LineSearchIter] = nonsdp.newton_method(FUN,obj.W,inner_tol,obj.options);
    obj.stats.finish('inner_iter');
    

    
    % Check if solver fails
    if obj.OUTFLAG < 0
        break
    end
    
    % Check feasibility
    check_feas(obj);
    FEASIBLE = (-obj.mat_feas < eps_mfeas) && (-obj.unfbar_feas < eps_mfeas);

    % Display
    if obj.options.display
        obj.printIter(it,NewtonCount,LineSearchIter,FVAL,inner_tol);
    end
    
    % Check for convergence
    if MINWEIGHT
        CONVERGED = true;
        obj.OUTFLAG = 2;
    else
        if abs((obj.objfuncval{1} - fval_0)./obj.objfuncval{1}) < inner_tol
            if gt(inner_tol,obj.options.outer_tol)
                inner_tol = max(inner_tol/10,obj.options.outer_tol);
            else
                CONVERGED = true;
            end
        end
    end
    % Check min barrier
    if all(obj.PA <= minPA) && all(obj.PB <= minPB) && all(obj.PC <= minPC) && all(obj.PC .* obj.PC2 <= minPC)
        MINWEIGHT = true;
        obj.OUTFLAG = 2;
        inner_tol = obj.options.outer_tol;
    end
    
    % Check convergence
    if FEASIBLE && CONVERGED
        break
    end
    
    % Update penalty and multiplier
    if obj.NA
        obj.updateMatrixMultipliers;
        obj.updateMatrixPenalty;
    end
    if obj.NB
        obj.updateMatrixBarrier;
    end
    if obj.NC
%         obj.updateLinearMultipliers;
        obj.updateLinearPenalty;
    end
end

if it==obj.options.max_outer_iter
    if FEASIBLE
        obj.OUTFLAG = -1;
    else
        obj.OUTFLAG = -2;
    end
end


obj.stats.finish('solve');
if obj.options.display
    printResults(obj.stats,obj.OUTFLAG,obj.OUTMSG,obj.options.check_derivatives);
end
end

function check_feas(obj)
    if obj.NA
        obj.mat_feas = nonsdp.matrixFeasibility(obj.matval{1});
    end
    if obj.NB
        obj.bar_feas = nonsdp.matrixFeasibility(obj.barval);
    end
    if obj.NC
        obj.unfbar_feas = nonsdp.matrixFeasibility(obj.unfbarval);
    end
end


function printHeader(name,NX,NA,NB,NC,NE)
% fprintf('This is NoN-SDP version %s\n',ver);
fprintf('Starting solve of %s at %s\n\n',name,datestr(now));
fprintf('%-30s%10d\n',   'Dimension  (parameter number):',NX);
if NE
    fprintf('%-30s%10d\n',   'Equalities  (linear-feasible):',NE);
end
if NA
    fprintf('%-30s%10d\n',   'Nonlinear matrix inequalities:',NA);
end
if NB
    fprintf('%-30s%10d\n',   'Linear barriered inequalities:',NB);
end
if NC
    fprintf('%-30s%10d\n',   'Linear penaltied inequalities:',NC);
end
fprintf('\n')
end



function printResults(stats,OUTERFLAG,OUTPUTMSG,dercheck)
switch OUTERFLAG
    case {0,-1,-2}
        fprintf(['\n',OUTPUTMSG,'\n']);
        fprintf('Optimal solution not found\n');
    case {1,2,3,4}
        fprintf(['\nOptimal solution found. ',OUTPUTMSG,'\n']);
        fprintf('%-40s%10.5f\n','Time required for init and solve:',stats.evaltime.init+stats.evaltime.solve);
        fprintf('%-40s%10.5f\n','Time required for initialisation:',stats.evaltime.init);
        fprintf('%-40s%10.5f\n','Time to compute optimal solution:',stats.evaltime.solve);
        fprintf('%-40s%10.5f\n','Average time per inner iteration:',stats.meantime.inner_iter);
        fprintf('%-40s%10.5f\n','Average time of lagr. evaluation:',stats.meantime.lagr);
        fprintf('%-40s%10d \n', 'Augmented lagrangian evaluations:',stats.numcalls.lagr);
        if dercheck
            fprintf('%-40s%10.5f\n\n', 'Time spent for derivatives check:',stats.evaltime.derivativecheck);
        else
            fprintf('\n');
        end
    case {-3,-4,-5,-6,-7,-8}
        fprintf(['\n',OUTPUTMSG,'\n']);
        fprintf('Optimisation aborted\n');
    otherwise
        fprintf('\nERROR: Wrong output flag. Unknown stop reason.\n');
end
end














