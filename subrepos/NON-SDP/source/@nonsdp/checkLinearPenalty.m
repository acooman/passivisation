function P = checkLinearPenalty(A,P,marg)
l = zeros(1,length(A));
for i=1:length(A)
    % Make sure eigenvalues are real
    l(i) = min(real(eig(A{i})));
end

% for i=1:length(A)
%    if - marg * l(i)  > P(i)
%        P(i) = marg * abs(l(i));
%    end
% end

if any(-marg*l > P(1))
    P = repmat(marg*abs(min(l)),[length(A),1]);
end

end
