function PA = checkMatrixPenalty(A,PA2,~,marg)
if iscell(A)
    l = zeros(1,length(A));
    for i=1:length(A)
        l(i) = max(-eig(A{i}));
    end
else
    l = -eig(A);
end
if any(marg*l > PA2)
    PA = marg*max(l);
else
    PA = PA2;
end
end
