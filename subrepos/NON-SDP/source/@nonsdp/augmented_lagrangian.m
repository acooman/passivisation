function [varargout] = augmented_lagrangian(obj,W)
% Validate derivatives
if obj.options.check_derivatives
    obj.stats.start('derivativecheck');
    obj.check_derivatives;
    obj.stats.finish('derivativecheck');
end
obj.stats.start('lagr');
% Number of desired outputs
N = nargout;

% Initialize  variables
NW  = length(W);
NX  = obj.Nx;

if obj.NE
    % Variable change
    W = obj.X0 + obj.KA * W;
end

% Initialise varargout
switch N
    case 1
        PENALTY = {0};
        BARRIER = {0};
        BARRPEN = {0};
        OBJFUNC = {0};
    case 2
        PENALTY = {0,zeros(1,NX)};
        BARRIER = {0,zeros(1,NX)};
        BARRPEN = {0,zeros(1,NX)};
        OBJFUNC = {0,zeros(1,NX)};

    case 3
        PENALTY = {0,zeros(1,NX),zeros(NX)};
        BARRIER = {0,zeros(1,NX),zeros(NX)};
        BARRPEN = {0,zeros(1,NX),zeros(NX)};
        OBJFUNC = {0,zeros(1,NX),zeros(NX)};
    otherwise
        error('Invalid number of outputs');
end

obj.matval = {};
if ~obj.linobj
    obj.objfuncval = {};
end
%% Compute matrix inequality
if obj.NA
    % Call matfun
    [obj.matval{1:N}] = obj.matfun(W);
    MAT_A = [obj.matval{:}];
    for i=1:obj.NA
        % Call penalty
        matrix = MAT_A(i,:);
        [FLAG,PEN{1:N}] = nonsdp.penalty(obj.PA,obj.UA{i},obj.penalty_change{i},matrix{:});
        if FLAG~=0
            varargout  = {inf,inf(1,NW),inf(NW)};
            return
        end
        % Add penalty to varargout
        PENALTY{1} =  PENALTY{1} + PEN{1};
        % Add barrier gradient to output cell array 
        if ge(N,2)
            ind = obj.GMAPA{i};
            PENALTY{2}(ind) = PENALTY{2}(ind) + PEN{2};
            % Add barrier hessian to output cell array (transform if necessary)
            if ge(N,3)
                PENALTY{3}(ind,ind) = PENALTY{3}(ind,ind) + PEN{3};
            end
        end
    end
end

%% Compute matrix barrier
for i=1:obj.NB
    % Compute matrix
    A = reshape(obj.matfeasible{i} * W(obj.GMAPB{i}) + obj.Bct{i},obj.SB(i),obj.SB(i));
    [FLAG,BAR{1:N}] = nonsdp.logdet(A,obj.matfeasible{i});
    if FLAG ~=0
        varargout  = {inf,inf(1,NW),inf(NW)};
        return
    end
    % Add penalty to varargout
    BARRIER{1} =  BARRIER{1} - obj.PB * BAR{1};
    % Add barrier gradient to output cell array (transform if necessary)
    if ge(N,2)
        ind = obj.GMAPB{i};
        BARRIER{2}(ind) = BARRIER{2}(ind) - obj.PB * BAR{2};
        % Add barrier hessian to output cell array (transform if necessary)
        if ge(N,3)
            BARRIER{3}(ind,ind) = BARRIER{3}(ind,ind) - obj.PB * BAR{3};
        end
    end
end

%% Compute modified matrix barrier
for i=1:obj.NC
    % Compute matrix
    A = reshape(obj.matunfeasible{i} * W(obj.GMAPC{i}) + obj.Cct{i},obj.SC(i),obj.SC(i));
    if obj.options.expand_inequalities
        [FLAG,BAR{1:N}] = nonsdp.logdet_mod_expanded_matrix(obj.PC(i),A);
    else
        [FLAG,BAR{1:N}] = nonsdp.logdet_mod(obj.PC(i),A,obj.matunfeasible{i},obj.matunfeasible_t{i});
    end
    if FLAG ~=0
        varargout  = {inf,inf(1,NW),inf(NW)};
        return
    end
    % Add penalty to varargout
    BARRPEN{1} =  BARRPEN{1} + obj.PC2(1) * BAR{1};
    % Add barrier gradient to output cell array (transform if necessary)
    if ge(N,2)
        ind = obj.GMAPC{i};
        BARRPEN{2}(ind) = BARRPEN{2}(ind) + obj.PC2(1) * BAR{2};
        % Add barrier hessian to output cell array (transform if necessary)
        if ge(N,3)
            BARRPEN{3}(ind,ind) = BARRPEN{3}(ind,ind) + obj.PC2(1) * BAR{3};
        end
    end
end
obj.barrpenval = BARRPEN;

%% Call objfun function
if obj.linobj
    % Add penalty to varargout
    OBJFUNC{1} =  OBJFUNC{1} + obj.objlingrad * W(obj.GMAPO);
    % Add barrier gradient to output cell array (transform if necessary)
    if ge(N,2)
        OBJFUNC{2}(obj.GMAPO) = OBJFUNC{2}(obj.GMAPO) + obj.objlingrad;
    end
else
    [OBJFUNC{1:N}] = obj.objfun(W);
    if ~isfinite(OBJFUNC{1})
        varargout  = {inf,inf(1,NW),inf(NW)};
        return
    end
end
obj.objfuncval = OBJFUNC;

%% Construct augmented lagrangian
varargout = cellfun(@(w,x,y,z) w+x+y+z,BARRIER,PENALTY,BARRPEN,OBJFUNC,'UniformOutput',false);


if obj.NE
    %% Variable change
    if N>1
        varargout{2} = varargout{2} * obj.KA;
        if N>2
            varargout{3} = obj.KA.' * varargout{3} * obj.KA;
        end
    end
end
obj.stats.finish('lagr');
end


