% Test barrier derivativefun
function [FLAG,F,G,H] = logdet(A,DA)
FLAG = 0;

% Cholesky factorization
[T,I]=chol(A);
if ~(all(isfinite(A(:)))) || I~=0
    FLAG = -1;
    F = inf;
    G = inf;
    H = inf;
    return;
end
% log of determinant
F = 2*sum(log(diag(T)));
% derivative
if nargout >1
    N  = size(A,1);
    Ai = A\eye(N);
    G  = reshape(Ai,1,[]) * DA;
    % Hessian
    if nargout >2
        Hp = kron(Ai,Ai);
        H  = - DA.' * Hp * DA;
    end
    
end