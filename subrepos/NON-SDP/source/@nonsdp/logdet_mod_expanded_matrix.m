% Test barrier derivativefun
function [FLAG,F,G,H] = logdet_mod_expanded_matrix(mu,A)
FLAG = 0;
% Make sure A is hermitian
A = (A+A')/2;
I = speye(size(A));
% Cholesky factorization
[T,pos]=chol(A./mu + I);
if pos~=0
    FLAG = -1;
    F = inf;     G = inf;     H = inf;
    return;
end
% log of determinant
F = - mu*2*sum(log(diag(T)));
% derivative
if nargout >2
    Ai = T\(T'\I);
    G  = - reshape(Ai,1,[]);
    % Hessian
    if nargout >3
        H = (1/mu) .* kron(Ai,Ai);
    end
end