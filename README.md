# Passive approximation of a system

**Problem statement:** Given a system $`A,B,C,D`$ on the unit circle, find the passive system $`A_p,B_p,C_p,D_p`$ such that the two systems are closest in least-squares sense.

## Simplified problem

The original problem is really difficult to solve. We can't do that yet. We can however keep the matrices $`A`$ and $`C`$ and find $`B_p,D_p`$ such that $`A,B_p,C,D_p`$ is passive. To find these $`B_p,D_p`$, we solve the following optimisation problem:

```math
\min_{B_p,D_p,P,Q,Z}t
```
```math
\left[\begin{array}{cc}
P-APA^{*} & -APC^{*}\\
-CPA^{*} & I-CPC^{*}
\end{array}\right] \geq Q
```
```math
\left[\begin{array}{cc}
Q & \left[\begin{array}{c}
B_p\\
D_p
\end{array}\right]\\
\left[\begin{array}{cc}
B_p^{*} & D_p^{*}\end{array}\right] & I
\end{array}\right]  \geq0
```
```math
\left[\begin{array}{cc}
Z & \left[\begin{array}{cc}
B^{*}-B_p^{*} & D^{*}-D_p^{*}\end{array}\right]\\
\left[\begin{array}{c}
B_-B_p\\
D_-D_p
\end{array}\right] & t\left[\begin{array}{cc}
W & 0\\
0 & I
\end{array}\right]^{-1}
\end{array}\right]  \geq0
```
```math
P \geq0
```
```math
\mathrm{trace}\left(Z\right)  \leq1
```

where $`W`$  is the solution of $`AWA^\prime-W+CC^\prime=0`$. We encounered numerical problems when $`W`$ is far from the identity matrix, so we first perform a similarity transform on the system $`A,B,C,D`$ such that its $`W`$ is close to the identity matrix.

## Functions

The repository contains the `makepassive` function to perform the above optimisation:

```matlab
  [Ap,Bp,Cp,Dp] = makepassive(A,B,C,D)
```

`makepassive` uses the nonsdp solver.

## Requirements

The nonSDP solver is not included in the repository. You have to install it yourself. It can be cloned at:

+ NonSDP: https://gitlab.inria.fr/MatchingToolbox/NoN-SDP

Besides the optimisers, you will need Matlab's systems and control toolbox.
The code has been tested in matlab 2017a
