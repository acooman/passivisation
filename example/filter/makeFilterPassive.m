clear variables
close all
clc

try
    cd(fileparts(mfilename('fullpath')));
catch
    warning('make sure you run this script from the example folder in the passive repo');
end

%% load the filter data
load ./filterapprox.mat


%% Use the makepassive function to compute a passive approximation
[Ap,Bp,Cp,Dp] = makepassive(A,B,C,D);

%% plot the results
Gorig = evaluateSystem(A  ,B  ,C  ,D  ,theta);
Gpass = evaluateSystem(Ap ,Bp ,Cp ,Dp ,theta);

figure(101)
clf
hold on
plot(squeeze(theta),db(reshapeforplot(data)),'g.');
plot(squeeze(theta),db(reshapeforplot(Gorig)),'b');
plot(squeeze(theta),db(reshapeforplot(Gpass)),'r');
title(sprintf('g=original data   b=rational approximation\n  r=passive approximation'));
grid on


%% small function to evaluate a state-space system without using other repos
function res = evaluateSystem(A,B,C,D,theta)
res = zeros(size(D,1),size(D,2),numel(theta));
for ff=1:numel(theta)
    res(:,:,ff) = C*((exp(1i*theta(ff)).*eye(size(A,1))-A)\B);
end
res = res+D;
end
function G=reshapeforplot(G)
[N,M,F]=size(G);
G=reshape(G,N*M,F);
end
function x = db(x)
x = 20*log10(abs(x));
end