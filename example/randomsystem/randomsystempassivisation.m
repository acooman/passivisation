clear variables
close all
clc

try
    error('a')
    N = 6;
    Ni= 2;
    No= 1;
    % generate the random system
    sys  = generateRandomSystem(...
        'numberOfComplexPoles',N,...
        'numberOfRealPoles',0,...
        'NumberOfInputs',Ni,...
        'NumberOfOutputs',No,...
        'SystemType','REAL',...
        'Domain','DISC',...
        'fmax',10,...
        'Qmax',10,'Qmin',2,...
        'transform',MobiusTransform('inside','LHP'));
    % get its ABCD matrices
    [A,B,C,D] = ssdata(sys);
    save randomsystem A B C D
catch
    load randomsystem
end

%% plot the original system response
theta = reshape(linspace(0,2*pi,10001),1,1,[]);
Gorig = evaluateSystem(A  ,B  ,C  ,D  ,theta);

% plot the result
figure(1)
clf
hold on
plot(squeeze(theta),db(reshapeforplot(Gorig)),'b','DisplayName','original system');
legend('show')
grid on

%% compute the passive approximation
tic
[Ap,Bp,Cp,Dp,optvars,constraints] = makepassive(A,B,C,D,'Display',true);
toc

%% evaluate and plot the result
Gpass = evaluateSystem(Ap ,Bp ,Cp ,Dp ,theta);

% plot the result
figure(1)
plot(squeeze(theta),db(reshapeforplot(Gpass)),'r','DisplayName','passive approximation');


%% small function to evaluate a state-space system without using other repos
function res = evaluateSystem(A,B,C,D,theta)
res = zeros(size(D,1),size(D,2),numel(theta));
for ff=1:numel(theta)
    res(:,:,ff) = C*((exp(1i*theta(ff)).*eye(size(A,1))-A)\B);
end
res = res+D;
end
function G=reshapeforplot(G)
[N,M,F]=size(G);
G=reshape(G,N*M,F);
end
function x = db(x)
x = 20*log10(abs(x));
end
