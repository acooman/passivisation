function [ Ap,Bp,Cp,Dp,optvars,constraints ] = makepassive( A,B,C,D, varargin)
%MAKEPASSIVE approximates a system in least-squares sense by a passive system
%
%   [Ap,Bp,Cp,Dp,optvars,constraints] = makepassive(A,B,C,D)
%
% where the original system has all its poles inside the unit circle and is 
% described by the state-space representation A,B,C,D
% 
% The Ap and Cp matrices are obtained from the original A and C by a similarity transformation such that
%   Ap'*Ap+Cp'*Cp = I
% The Bp and Dp are then chosen such that the system A,Bp,C,Dp is passive.
%
%   optvars is a struct which contains the optimisation variables
%   optvars.t  least-squares error between the two systems 
%   optvars.Bp B-matrix of the passive approximation
%   optvars.Dp D-matrix of the passive approximation
%   optvars.P 
%   optvars.Z
%   optvars.Q
% 
%   constraints is a struct with the constraint matrices after optimisation
%   constraints.PQpos
%                      [ P-A*P*A'   -A*P*C' ]
%                      [  -C*P*A'  I-C*P*C' ] - Q >= 0   
%   constraints.Qpos contains the matrix
%                      [ Q     B ]
%                      [       D ] >= 0
%                      [ B' D' I ]
%   constraints.Zpos contains the matrix
%                      [ Z     B'-Bp' D'-Dp' ]
%                      [ B-Bp                ] >= 0
%                      [ D-Dp       t*Xinv   ]
%   constraints.Ppos contains the matrix P
%   constraints.traZ contains the constraint 1-trace(Z)>=0
%
% This function requires a copy of the Non-SDP optimiser on the matlab path
% You can clone the optimiser at
%
%   https://gitlab.inria.fr/MatchingToolbox/NoN-SDP 

if nargin>=4
    [N,No,Ni]=validateABCD(A,B,C,D);
else
    error('You need to specify the A,B,C and D matrices');
end

p=inputParser();
% domain of the state-space system
p.addParameter('Domain','DISC',@(x) any(strcmpi(x,{'PLANE','DISC'})));
% Frequency normalisation used on the system
p.addParameter('Normalisation',LowpassNormalisation(-1,1),@(x) isa(x,'Normalisation'));
% Transform to map the system from the complex plane to the disc
p.addParameter('Transform',MobiusTransform('Inside','LHP'),@(x) isa(x,'Transform'));
% set this to true to show the nonsdp info
p.addParameter('Display',false,@islogical);
% nonsdp solver options. When you set this, the display option is
% overwritten by whatever nonopt.Display has
p.addParameter('nonopt',[],@(x) isa(x,'nonopt'));
p.parse(varargin{:});
args = p.Results;

% go to the disc if needed
if strcmpi(args.Domain,'PLANE')
    % normalise
    [A,B,C,D]=Forward_ABCD(args.Normalisation,A,B,C,D);
    % go to the disc
    [A,B,C,D]=Forward_ABCD(args.Transform,A,B,C,D);
end

% check whether the system has all its poles inside the unit circle
if any(abs(eig(A))>=1)
    error('The system has poles outside of the unit circle');
end

% balance the system for good numerical conditioning
[A,B,C,D,Winv] = balanceSystem(A,B,C,D);

% gather the constraints in the con matrix
con.A = A;
con.B = B;
con.C = C;
con.D = D;
con.Winv = Winv;

if isreal([A B;C D])
    % random initial values
    % make sure P,Z and Q are symmetric
    optvars.t = 1;
    optvars.B = rand(N,Ni);
    optvars.D = rand(No,Ni);
    optvars.P = rand(N,N);
    optvars.P = optvars.P*optvars.P.';
    optvars.Z = 0.1*rand(Ni,Ni);
    optvars.Z = optvars.Z*optvars.Z.';
    optvars.Q = rand(N+No,N+No);
    optvars.Q = optvars.Q*optvars.Q.';
    
    % gather the initial values in the parameter vector
    X0 = putMatricesIntoParamVector(optvars);
    
    % compute the derivative of the different matrices with respect to the parameter vector
    der=getDerMatricesFromParamVector(length(X0),N,Ni,No);
    
    % [ Q     B ]
    % [       D ] >= 0
    % [ B' D' I ]
    [C0{    1,1},dC{    1,1},GMAPC{    1,1}] = Qpos(optvars,con,N,Ni,No,der);
    % [ Z       Bhat'-B' Dhat'-D' ]
    % [ Bhat-B                    ] >= 0
    % [ Dhat-D           t*Xinv   ]
    [C0{end+1,1},dC{end+1,1},GMAPC{end+1,1}] = Zpos(optvars,con,N,Ni,No,der);
    % [ P-A*P*A'   -A*P*C' ]
    % [  -C*P*A'  I-C*P*C' ] - Q >= 0
    [C0{end+1,1},dC{end+1,1},GMAPC{end+1,1}] = PQpos(optvars,con,N,Ni,No,der);
    % 1 - trace(Z) >= 0
    [C0{end+1,1},dC{end+1,1},GMAPC{end+1,1}] = traZ(optvars,con,N,Ni,No,der);
    % P >=0
    [C0{end+1,1},dC{end+1,1},GMAPC{end+1,1}] = Ppos(optvars,con,N,Ni,No,der);
    
    % apply the usage map to make the derivative matrices smaller
    dC = cellfun(@(dC,GMAPC) dC(:,GMAPC) , dC,GMAPC ,'uniformOutput',false);
else
    % random initial values
    % make sure P,Z and Q are symmetric
    optvars.t = 1;
    optvars.B = rand(N,Ni)+1i*rand(N,Ni);
    optvars.D = rand(No,Ni)+1i*rand(No,Ni);
    optvars.P = rand(N,N)+1i*rand(N,N);
    optvars.P = optvars.P*optvars.P';
    optvars.Z = rand(Ni,Ni)+1i*rand(Ni,Ni);
    optvars.Z = optvars.Z*optvars.Z';
    optvars.Q = rand(N+No,N+No)+1i*rand(N+No,N+No);
    optvars.Q = optvars.Q*optvars.Q';
    
    % gather the initial values in the parameter vector
    X0 = putMatricesIntoParamVectorComplex(optvars);
    
%     test = getMatricesFromParamVectorComplex(X0,N,Ni,No);
%     fields = fieldnames(test);
%     for ff=1:length(fields)
%         if any(abs(optvars.(fields{ff})(:)-test.(fields{ff})(:))>1e-16)
%             error('sjdfh')
%         end
%     end
    
    % compute the derivative of the different matrices with respect to the parameter vector
    der=getDerMatricesFromParamVectorComplex(length(X0),N,Ni,No);
    
    % [ Q     B ]
    % [       D ] >= 0
    % [ B' D' I ]
    [C0{    1,1},dC{    1,1},GMAPC{    1,1}] = QposComplex(optvars,con,N,Ni,No,der);
    % [ Z       Bhat'-B' Dhat'-D' ]
    % [ Bhat-B                    ] >= 0
    % [ Dhat-D           t*Xinv   ]
    [C0{end+1,1},dC{end+1,1},GMAPC{end+1,1}] = ZposComplex(optvars,con,N,Ni,No,der);
    % [ P-A*P*A'   -A*P*C' ]
    % [  -C*P*A'  I-C*P*C' ] - Q >= 0
    [C0{end+1,1},dC{end+1,1},GMAPC{end+1,1}] = PQposComplex(optvars,con,N,Ni,No,der);
    % trace(Z) >= 0
    [C0{end+1,1},dC{end+1,1},GMAPC{end+1,1}] = traZComplex(optvars,con,N,Ni,No,der);
    % P >=0
    [C0{end+1,1},dC{end+1,1},GMAPC{end+1,1}] = PposComplex(optvars,con,N,Ni,No,der);
        
    % apply the usage map to make the derivative matrices smaller
    dC = cellfun(@(dC,GMAPC) dC(:,GMAPC) , dC , GMAPC ,'uniformOutput',false);
end

if isempty(args.nonopt)
    options = nonopt();
    options.display = args.Display;
    options.outer_tol = 1e-8;
    options.max_outer_iter = 500;
else
    options = args.nonopt;
end


% optimise
solver = nonsdp('X0',X0,'objfun',@(x) objfun(x),'C0',C0,'matunfeasible',dC,'GMAPC',GMAPC.','options',options);
solver.solve;

switch solver.OUTFLAG
    case {1,2,3,4}
        % do nothing, all is well
    case {-1,5}
        fprintf('optimisation did not end normally.\n reason: %s\n',solver.OUTMSG);
    otherwise
        error('optimisation did not converge.\n reason: %s',solver.OUTMSG);
end

% get the solution out of the parameter vector
if isreal([A,B;C,D])
    optvars = getMatricesFromParamVector(solver.x,N,Ni,No);
else
    optvars = getMatricesFromParamVectorComplex(solver.x,N,Ni,No);
end

% get the constraints value in the optimum
T=solver.unfbarval;
constraints.Qpos = T{1};
constraints.Zpos = T{2};
constraints.PQpos= T{3};
constraints.traZ = T{4};
constraints.Ppos = T{5};

% get the results together
Ap = con.A;
Bp = optvars.B;
Cp = con.C;
Dp = optvars.D;

% go to the disc if needed
if strcmpi(args.Domain,'PLANE')
    % go to the plane
    [Ap,Bp,Cp,Dp]=Backward_ABCD(args.Transform,Ap,Bp,Cp,Dp);
    % normalise
    [Ap,Bp,Cp,Dp]=Backward_ABCD(args.Normalisation,Ap,Bp,Cp,Dp);
end

end



%% objfun, barfun and matfun
function [f,df,ddf]=objfun(X)
f = X(1);
df = zeros(size(X)).';
df(1)=1;
ddf=[];
end
% function [C,dC,ddC]=matfun(X,con,dC,der)
%     N  = size(con.A,1);
%     Ni = size(con.B,2);
%     No = size(con.C,1);
%     optvars = getMatricesFromParamVectorComplex(X,N,Ni,No);
%     % [ Q     B ]
%     % [       D ] >= 0
%     % [ B' D' I ]
%     [C{    1,1}] = QposComplex(optvars,con,N,Ni,No,der);
%     % [ Z       Bhat'-B' Dhat'-D' ]
%     % [ Bhat-B                    ] >= 0
%     % [ Dhat-D           t*Xinv   ]
%     [C{end+1,1}] = ZposComplex(optvars,con,N,Ni,No,der);
%     % [ P-A*P*A'   -A*P*C' ]
%     % [  -C*P*A'  I-C*P*C' ] - Q >= 0
%     [C{end+1,1}] = PQposComplex(optvars,con,N,Ni,No,der);
%     % trace(Z) >= 0
%     [C{end+1,1}] = traZComplex(optvars,con,N,Ni,No,der);
%     % P >=0
%     [C{end+1,1}] = PposComplex(optvars,con,N,Ni,No,der);
%         
%     % put [] as second derivative to indicate that the thing is linear
%     ddC=cell(size(C));
%     [ddC{:}]=deal([]);
% end
%% Ppos
function [R,dR,use]=Ppos(mat,con,N,Ni,No,der)
% P >=0 
R = mat.P;
% compute the derivative
dR=full(der.P);
% compute the usage map
mat.t = zeros(1,1 );
mat.B = zeros(N,Ni);
mat.D = zeros(No,Ni);
mat.P = ones(N,N);
mat.Z = zeros(Ni,Ni);
mat.Q = zeros(N+No,N+No);
use= (putMatricesIntoParamVector(mat)==1).';
end
function [R,dR,use]=PposComplex(mat,con,N,Ni,No,der)
% P >=0 
R = mat.P;
% compute the derivative
dR=full(der.P);
% compute the usage map
mat.t = zeros(1,1 );
mat.B = zeros(N,Ni);
mat.D = zeros(No,Ni);
mat.P = ones(N,N)+1i*ones(N,N);
mat.Z = zeros(Ni,Ni);
mat.Q = zeros(N+No,N+No);
use= (putMatricesIntoParamVectorComplex(mat)==1).';
end
%% Qpos
function [R,dR,use]=Qpos(mat,con,N,Ni,No,der)
% [ Q     B ]
% [       D ] >= 0
% [ B' D' I ] 
BD = [mat.B;mat.D];
R = [mat.Q , BD; BD' , eye(Ni)];
% compute the derivative
dQ = submatrixDer([N+No+Ni N+No+Ni],[     1 1     ],[N+No N+No])*der.Q;
dB = submatrixDer([N+No+Ni N+No+Ni],[     1 N+No+1],[N  Ni])*der.B;
dD = submatrixDer([N+No+Ni N+No+Ni],[   N+1 N+No+1],[No Ni])*der.D;
dBt= submatrixDer([N+No+Ni N+No+Ni],[N+No+1 1     ],[Ni N ])*vecperm([N  Ni])*der.B;
dDt= submatrixDer([N+No+Ni N+No+Ni],[N+No+1 N+1   ],[Ni No])*vecperm([No Ni])*der.D;
dR = full(dQ + dB + dD + dBt + dDt);
% compute the usage map
mat.t = zeros(1,1 );
mat.B = ones(N,Ni);
mat.D = ones(No,Ni);
mat.P = zeros(N,N);
mat.Z = zeros(Ni,Ni);
mat.Q = ones(N+No,N+No);
use= (putMatricesIntoParamVector(mat)==1).';
end
function [R,dR,use]=QposComplex(mat,con,N,Ni,No,der)
% [ Q     B ]
% [       D ] >= 0
% [ B' D' I ] 
BD = [mat.B;mat.D];
R = [mat.Q , BD; BD' , eye(Ni)];
% compute the derivative
dQ = submatrixDer([N+No+Ni N+No+Ni],[     1 1     ],[N+No N+No])*der.Q;
dB = submatrixDer([N+No+Ni N+No+Ni],[     1 N+No+1],[N  Ni])*der.B;
dD = submatrixDer([N+No+Ni N+No+Ni],[   N+1 N+No+1],[No Ni])*der.D;
dBt= submatrixDer([N+No+Ni N+No+Ni],[N+No+1 1     ],[Ni N ])*vecperm([N  Ni])*conj(der.B);
dDt= submatrixDer([N+No+Ni N+No+Ni],[N+No+1 N+1   ],[Ni No])*vecperm([No Ni])*conj(der.D);
dR = full(dQ + dB + dD + dBt + dDt);
% compute the usage map
mat.t = zeros(1,1 );
mat.B = ones(N,Ni)+1i*ones(N,Ni);
mat.D = ones(No,Ni)+1i*ones(No,Ni);
mat.P = zeros(N,N);
mat.Z = zeros(Ni,Ni);
mat.Q = ones(N+No,N+No)+1i*ones(N+No,N+No);
use= (putMatricesIntoParamVectorComplex(mat)==1).';
end
%% PQpos
function [R,dR,use]=PQpos(mat,con,N,Ni,No,der)
% [ P-A*P*A'   -A*P*C' ]   
% [  -C*P*A'  I-C*P*C' ] - Q >= 0
R = [mat.P-con.A*mat.P*(con.A.') , -con.A*mat.P*(con.C.'); -con.C*mat.P*(con.A.') , eye(No,No)-con.C*mat.P*(con.C.') ]-mat.Q;
% compute the derivative
% we use vec(ABC) = (Ct kron A)vec(B) here
dP11=submatrixDer([N+No N+No],[1     1],[N  N ])*(eye(N*N)+kron(con.A,-con.A))*der.P;
dP12=submatrixDer([N+No N+No],[1   N+1],[N  No])*(kron(con.C,-con.A))*der.P;
dP21=submatrixDer([N+No N+No],[N+1   1],[No N ])*(kron(con.A,-con.C))*der.P;
dP22=submatrixDer([N+No N+No],[N+1 N+1],[No No])*(kron(con.C,-con.C))*der.P;
dQ =-der.Q;
dR = full(dP11 + dP12 + dP21 + dP22 + dQ);
% compute the usage map
mat.t = zeros(1,1 );
mat.B = zeros(N,Ni);
mat.D = zeros(No,Ni);
mat.P = ones(N,N);
mat.Z = zeros(Ni,Ni);
mat.Q = ones(N+No,N+No);
use= (putMatricesIntoParamVector(mat)==1).';
end
function [R,dR,use]=PQposComplex(mat,con,N,Ni,No,der)
% [ P-A*P*A'   -A*P*C' ]   
% [  -C*P*A'  I-C*P*C' ] - Q >= 0
APC = con.A*mat.P*(con.C');
R = [mat.P-con.A*mat.P*(con.A') , -APC; -APC' , eye(No,No)-con.C*mat.P*(con.C') ]-mat.Q;
% make sure R is really hermitian
R = (R+R')/2;
% compute the derivative
% we use vec(ABC) = (Ct kron A)vec(B) here
dP11=submatrixDer([N+No N+No],[1     1],[N  N ])*(eye(N*N)+kron(conj(con.A),-con.A))*der.P;
dP12=submatrixDer([N+No N+No],[1   N+1],[N  No])*(kron(conj(con.C),-con.A))*der.P;
dP21=submatrixDer([N+No N+No],[N+1   1],[No N ])*(kron(conj(con.A),-con.C))*der.P;
dP22=submatrixDer([N+No N+No],[N+1 N+1],[No No])*(kron(conj(con.C),-con.C))*der.P;
dQ =-der.Q;
dR = full(dP11 + dP12 + dP21 + dP22 + dQ);
% compute the usage map
mat.t = zeros(1,1 );
mat.B = zeros(N,Ni);
mat.D = zeros(No,Ni);
mat.P = ones(N,N)+1i*ones(N,N);
mat.Z = zeros(Ni,Ni);
mat.Q = ones(N+No,N+No)+1i*ones(N+No,N+No);
use= (putMatricesIntoParamVectorComplex(mat)==1).';
end
%% Zpos
function [R,dR,use]=Zpos(mat,con,N,Ni,No,der)
% [ Z       Bhat'-B' Dhat'-D' ]
% [ Bhat-B                    ] >= 0
% [ Dhat-D           t*Xinv   ]
Xinv = [con.Winv zeros(N,No);zeros(No,N) eye(No)];
BD = [mat.B;mat.D]-[con.B;con.D];
R = [mat.Z -BD.';-BD mat.t*Xinv];
% derivative
dZ = submatrixDer([N+No+Ni N+No+Ni],[1      1     ],[Ni   Ni])*der.Z;
dB =-submatrixDer([N+No+Ni N+No+Ni],[Ni+1   1     ],[N    Ni])*der.B;
dD =-submatrixDer([N+No+Ni N+No+Ni],[Ni+N+1 1     ],[No   Ni])*der.D;
dBt=-submatrixDer([N+No+Ni N+No+Ni],[1      Ni+1  ],[Ni   N ])*vecperm([N  Ni])*der.B;
dDt=-submatrixDer([N+No+Ni N+No+Ni],[1      Ni+N+1],[Ni   No])*vecperm([No Ni])*der.D;
dt = submatrixDer([N+No+Ni N+No+Ni],[Ni+1   Ni+1  ],[N+No N+No])*Xinv(:)*der.t;
dR= full(dZ + dB + dD + dBt + dDt + dt);
% compute the usage map
mat.t = ones(1,1 );
mat.B = ones(N,Ni);
mat.D = ones(No,Ni);
mat.P = zeros(N,N);
mat.Z = ones(Ni,Ni);
mat.Q = zeros(N+No,N+No);
use= (putMatricesIntoParamVector(mat)==1).';
end
function [R,dR,use]=ZposComplex(mat,con,N,Ni,No,der)
% [ Z       Bhat'-B' Dhat'-D' ]
% [ Bhat-B                    ] >= 0
% [ Dhat-D           t*Xinv   ]
Xinv = [con.Winv zeros(N,No);zeros(No,N) eye(No)];
BD = [mat.B;mat.D]-[con.B;con.D];
R = [mat.Z -BD';-BD mat.t*Xinv];
% derivative
dZ = submatrixDer([N+No+Ni N+No+Ni],[1      1     ],[Ni   Ni])*der.Z;
dB =-submatrixDer([N+No+Ni N+No+Ni],[Ni+1   1     ],[N    Ni])*der.B;
dD =-submatrixDer([N+No+Ni N+No+Ni],[Ni+N+1 1     ],[No   Ni])*der.D;
dBt=-submatrixDer([N+No+Ni N+No+Ni],[1      Ni+1  ],[Ni   N ])*vecperm([N  Ni])*conj(der.B);
dDt=-submatrixDer([N+No+Ni N+No+Ni],[1      Ni+N+1],[Ni   No])*vecperm([No Ni])*conj(der.D);
dt = submatrixDer([N+No+Ni N+No+Ni],[Ni+1   Ni+1  ],[N+No N+No])*Xinv(:)*der.t;
dR= full(dZ + dB + dD + dBt + dDt + dt);
% compute the usage map
mat.t = ones(1,1 );
mat.B = ones(N,Ni)+1i*ones(N,Ni);
mat.D = ones(No,Ni)+1i*ones(No,Ni);
mat.P = zeros(N,N);
mat.Z = ones(Ni,Ni)+1i*ones(Ni,Ni);
mat.Q = zeros(N+No,N+No);
use= (putMatricesIntoParamVectorComplex(mat)==1).';
end
%% trace(Z)
function [R,dR,use]=traZ(mat,con,N,Ni,No,der)
% trace(Z) >= 0
R = 1-trace(mat.Z);
% derivative
dR=-full(traceDer([Ni Ni])*der.Z);
% compute the usage map
mat.t = zeros(1,1 );
mat.B = zeros(N,Ni);
mat.D = zeros(No,Ni);
mat.P = zeros(N,N);
mat.Z = eye(Ni,Ni);
mat.Q = zeros(N+No,N+No);
use= (putMatricesIntoParamVector(mat)==1).';
end
function [R,dR,use]=traZComplex(mat,con,N,Ni,No,der)
% trace(Z) >= 0
R = 1-trace(mat.Z);
% derivative
dR=-full(traceDer([Ni Ni])*der.Z);
% compute the usage map
mat.t = zeros(1,1 );
mat.B = zeros(N,Ni);
mat.D = zeros(No,Ni);
mat.P = zeros(N,N);
mat.Z = eye(Ni,Ni)+1i*eye(Ni,Ni);
mat.Q = zeros(N+No,N+No);
use= (putMatricesIntoParamVectorComplex(mat)==1).';
end
%% functions to put matrices into the parameter vector
function X = putMatricesIntoParamVector(mat)
X=[ mat.t;
    mat.B(:);
    mat.D(:);
    mat.P(tril(true(size(mat.P))));
    mat.Z(tril(true(size(mat.Z))));
    mat.Q(tril(true(size(mat.Q))))];
end
function X = putMatricesIntoParamVectorComplex(mat)
X=[ mat.t;
    real(mat.B(:));
    imag(mat.B(:));
    real(mat.D(:));
    imag(mat.D(:));
    real(mat.P(tril(true(size(mat.P)))));
    imag(mat.P(tril(true(size(mat.P)),-1)));
    real(mat.Z(tril(true(size(mat.Z)))));
    imag(mat.Z(tril(true(size(mat.Z)),-1)));
    real(mat.Q(tril(true(size(mat.Q)))));
    imag(mat.Q(tril(true(size(mat.Q)),-1)))];
end
%% functions to get matrices from the parameter vector
function [mat]=getMatricesFromParamVector(X,N,Ni,No)
[ind,mat.t]=getNextMatrix   (X,   1,   1,  0);
[ind,mat.B]=getNextMatrix   (X,   N,  Ni,ind);
[ind,mat.D]=getNextMatrix   (X,  No,  Ni,ind);
[ind,mat.P]=getNextSymMatrix(X,   N,   N,ind);
[ind,mat.Z]=getNextSymMatrix(X,  Ni,  Ni,ind);
[  ~,mat.Q]=getNextSymMatrix(X,N+No,N+No,ind);
end
function [mat]=getMatricesFromParamVectorComplex(X,N,Ni,No)
[ind,mat.t]=getNextMatrix          (X,   1,   1,  0);
[ind,mat.B]=getNextMatrixComplex   (X,   N,  Ni,ind);
[ind,mat.D]=getNextMatrixComplex   (X,  No,  Ni,ind);
[ind,mat.P]=getNextHermMatrix(X,   N,   N,ind);
[ind,mat.Z]=getNextHermMatrix(X,  Ni,  Ni,ind);
[  ~,mat.Q]=getNextHermMatrix(X,N+No,N+No,ind);
end
%% functions to get the derivative of a matrix with respect to the parameter vector
function [der]=getDerMatricesFromParamVector(Nx,N,Ni,No)
[ind,der.t]=getDerNextMatrix   (Nx,   1,   1,  0);
[ind,der.B]=getDerNextMatrix   (Nx,   N,  Ni,ind);
[ind,der.D]=getDerNextMatrix   (Nx,  No, Ni,ind);
[ind,der.P]=getDerNextSymMatrix(Nx,   N,   N,ind);
[ind,der.Z]=getDerNextSymMatrix(Nx,  Ni,  Ni,ind);
[  ~,der.Q]=getDerNextSymMatrix(Nx,N+No,N+No,ind);
end
function [der]=getDerMatricesFromParamVectorComplex(Nx,N,Ni,No)
[ind,der.t]=getDerNextMatrix       (Nx,   1,   1,  0);
[ind,der.B]=getDerNextMatrixComplex(Nx,   N,  Ni,ind);
[ind,der.D]=getDerNextMatrixComplex(Nx,  No,  Ni,ind);
[ind,der.P]=getDerNextHermMatrix   (Nx,   N,   N,ind);
[ind,der.Z]=getDerNextHermMatrix   (Nx,  Ni,  Ni,ind);
[  ~,der.Q]=getDerNextHermMatrix   (Nx,N+No,N+No,ind);
end
%% function to get a full matrix and its derivative from the parameter vector
function [ind,M]=getNextMatrix(X,rows,cols,ind)
rng = (ind+1):(ind+rows*cols);
M =reshape(X(rng),rows,cols);
ind = ind+cols*rows;
end
function [ind,M]=getNextMatrixComplex(X,rows,cols,ind)
[ind,ReM] = getNextMatrix(X,rows,cols,ind);
[ind,ImM] = getNextMatrix(X,rows,cols,ind);
M = ReM + 1i*ImM;
end
function [ind,dvecMdX]=getDerNextMatrix(Nx,rows,cols,ind)
rng = (ind+1):(ind+rows*cols);
dvecMdX = sparse([],[],[],rows*cols,Nx,rows*cols);
dvecMdX(:,rng) = speye(rows*cols);
ind = ind+cols*rows;
end
function [ind,dvecMdX]=getDerNextMatrixComplex(Nx,rows,cols,ind)
[ind,RedvecMdX]=getDerNextMatrix(Nx,rows,cols,ind);
[ind,ImdvecMdX]=getDerNextMatrix(Nx,rows,cols,ind);
dvecMdX = RedvecMdX + 1i*ImdvecMdX;
end
%% functions to get a symmetric matrix from the parameter vector
function [ind,M]=getNextSymMatrix(X,N,~,ind)
m = X(ind+1:ind+N*(N+1)/2);
ind = ind+N*(N+1)/2;
low=tril(true(N));
inds=double(low);
inds(low)=1:length(m);
inds = inds + triu(inds.',1);
M = m(inds);
end
function [ind,dvecMdX]=getDerNextSymMatrix(Nx,N,~,ind)
low=tril(true(N));
inds=double(low);
inds(low)=1:(N*(N+1)/2);
inds = inds + triu(inds.',1);
dvecMdX = sparse(1:N*N,inds,1,N*N,N*(N+1)/2,N*N);
dvecMdX = [sparse([],[],[],N^2,ind) dvecMdX sparse([],[],[],N^2,Nx-ind-N*(N+1)/2)];
ind = ind+N*(N+1)/2;
end
%% functions to recover the imaginary part of an hermitian matrix
function [ind,M]=getNextMatNoDiag(X,N,~,ind)
low=tril(true(N),-1);
inds=double(low);
Npar = sum(inds(:));
m = [0;X(ind+1:ind+Npar)];
inds(low)=1:Npar;
M = m(inds+1);
M = M - M.';
ind = ind+Npar;
end
function [ind,dvecMdX]=getDerNextMatNoDiag(Nx,N,~,ind)
low=tril(true(N),-1);
inds=double(low);
Npar = sum(inds(:));
inds(low)=1:Npar;
inds = inds - inds.';
row = 1:N*N;
col = abs(inds(:));
val = sign(inds(:));
dvecMdX = sparse(row(col~=0),col(col~=0),val(col~=0),N*N,Npar);
dvecMdX = [sparse([],[],[],N^2,ind) dvecMdX sparse([],[],[],N^2,Nx-ind-Npar)];
ind = ind+Npar;
end
%% functions to get a Hermitian matrix and its derivative from the parameter vector
function [ind,M]=getNextHermMatrix(X,N,~,ind)
% the real part is easy
[ind,ReM] = getNextSymMatrix(X,N,[],ind);
% the imaginary part is a little bit more tricky, because we skip the diagonal
[ind,ImM] = getNextMatNoDiag(X,N,[],ind);
% combine the real and imaginary parts
M = ReM + 1i*ImM;
end
function [ind,dvecMdX]=getDerNextHermMatrix(Nx,N,~,ind)
[ind,RedvecMdX]=getDerNextSymMatrix(Nx,N,[],ind);
[ind,ImdvecMdX]=getDerNextMatNoDiag(Nx,N,[],ind);
dvecMdX = RedvecMdX + 1i*ImdvecMdX;
end

%% derivative functions
function D=submatrixDer(largematsize,start,smallmatsize)
numelmat = prod(largematsize);
numelpar = prod(smallmatsize);
T = sparse([],[],[],largematsize(1),largematsize(2),numelpar);
T(start(1):start(1)+smallmatsize(1)-1,start(2):start(2)+smallmatsize(2)-1)=1;
D = sparse(find(T),1:numelpar,ones(numelpar,1),numelmat,numelpar);
end
function D = traceDer(size)
D = eye(size);
D = D(:).';
end
function P = vecperm(size)
m = size(1);
n = size(2);
P = zeros(prod(size));
I = eye(m*n);
k = 1;
for i=1:n
    for j=i:n:m*n
        P(k,:) = I(j,:);
        k = k+1;
    end
end
P=P.';
end

function [N,No,Ni]=validateABCD(A,B,C,D)
validateattributes(A,{'numeric'},{'square','finite','nonnan'});
N = size(A,1);
validateattributes(B,{'numeric'},{'size',[N NaN],'finite','nonnan'});
No = size(C,1);
validateattributes(C,{'numeric'},{'size',[NaN N],'finite','nonnan'});
Ni = size(B,2);
validateattributes(D,{'numeric'},{'size',[No Ni],'finite','nonnan'});
end

