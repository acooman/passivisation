function run_passivisation_tests()

% go to the right folder
fi = mfilename('fullpath');
path = fileparts([fi,'.m']);
cd(path)
cd ..

import matlab.unittest.TestSuite
import matlab.unittest.TestRunner
import matlab.unittest.plugins.CodeCoveragePlugin
% import matlab.unittest.plugins.StopOnFailuresPlugin

codeFolders = {fullfile(pwd)};
suite = TestSuite.fromFile(fullfile('test','PassivisationTest.m'));
runner = TestRunner.withTextOutput;
runner.addPlugin(CodeCoveragePlugin.forFolder(codeFolders));
% runner.addPlugin(StopOnFailuresPlugin)
result = runner.run(suite);

end

