classdef PassivisationTest < matlab.unittest.TestCase
    properties
        yalmippath = 'C:\Users\acooman\Documents\YALMIP-master';
        sdpt3path = 'C:\Users\acooman\Documents\SDPT3';
        tol = 1e-2;
        check = 'tonly';
    end
    
    properties (TestParameter)
        domain = {'PLANE','DISC'};
        No = struct('one', 1,'two', 2, 'three', 3);
        Ni = struct('one', 1,'two', 2, 'three', 3);
        N  = struct('zero', 2,'four', 4, 'ten', 10);
        systemType = {'real','complex'};
    end
    
    methods (Test)
        function compareToYalmip(testCase,No,Ni,N,systemType,domain)
            Transform = MobiusTransform('Inside','LHP');
            % generate the random system
            sys  = generateRandomSystem('numberOfComplexPoles',N,...
                                        'numberOfRealPoles',0,...
                                        'NumberOfInputs',Ni,...
                                        'NumberOfOutputs',No,...
                                        'SystemType',systemType,...
                                        'Domain',domain,...
                                        'fmax',10,...
                                        'Qmax',10,'Qmin',2,...
                                        'transform',Transform);
            % generate the system's normalisation
            Normalisation = LowpassNormalisation(-10,10);
            % get the system's ABCD matrices
            [A,B,C,D] = ssdata(sys);
            % plot the original system
            if strcmpi(domain,'PLANE')
                freq=linspace(-10,10,10001);
                G = FRM(freqresp(sys,freq,'Hz'),'Freq',freq,'Domain',domain);
            else
                theta=linspace(0,2*pi,10001);
                G = FRM(freqresp(sys,exp(1i*theta)),'Freq',theta,'Domain',domain);
            end
            figure(1)
            clf
            grid on
            legend('show')
            plot(G,'Color',[0 100/255 0],'LineStyle','-','DisplayName','original');
            % compute passive approximation
            try
                tic
                [Ap,Bp,Cp,Dp,optvars,constraints] = makepassive(A,B,C,D,'Normalisation',Normalisation,'Domain',domain,'Display',false);
                tnonsdp = toc;
                % plot the result
                if strcmpi(domain,'PLANE')
                    Gp = FRM(freqresp(StateSpace(Ap,Bp,Cp,Dp),freq,'Hz'),'Freq',freq,'Domain',domain);
                else
                    Gp = FRM(freqresp(StateSpace(Ap,Bp,Cp,Dp),exp(1i*theta)),'Freq',theta,'Domain',domain);
                end
                figure(1)
                plot(Gp,'Color','r','LineStyle','-','DisplayName','nonsdp');
            catch err
                keyboard
            end
            % compute the same passive approximation with yalmip
            try
                addpath(genpath(testCase.yalmippath));
                addpath(genpath(testCase.sdpt3path));
                tic
                [Apy,Bpy,Cpy,Dpy,optvarsy,constraintsy] = makepassive_yalmip(A,B,C,D,'Normalisation',Normalisation,'Domain',domain);
                tyalmip = toc;
                rmpath(genpath(testCase.sdpt3path));
                rmpath(genpath(testCase.yalmippath));
                % plot the result
                if strcmpi(domain,'PLANE')
                    Gpy = FRM(freqresp(StateSpace(Apy,Bpy,Cpy,Dpy),freq,'Hz'),'Freq',freq,'Domain',domain);
                else
                    Gpy = FRM(freqresp(StateSpace(Apy,Bpy,Cpy,Dpy),exp(1i*theta)),'Freq',theta,'Domain',domain);
                end
                figure(1)
                plot(Gp,'Color','b','LineStyle','--','DisplayName','yalmip');
            catch err
                keyboard
            end
            % force showing of the figure
            figure(1)
            drawnow
            % compare the results
            switch lower(testCase.check)
                case 'all'
                    testCase.verifyEqual(Ap,Apy,'reltol',testCase.tol);
                    testCase.verifyEqual(Bp,Bpy,'reltol',testCase.tol);
                    testCase.verifyEqual(Cp,Cpy,'reltol',testCase.tol);
                    testCase.verifyEqual(Dp,Dpy,'reltol',testCase.tol);
                    fields = fieldnames(optvars);
                    for ff=1:length(fields)
                        testCase.verifyEqual(optvars.(fields{ff}),optvarsy.(fields{ff}),'reltol',testCase.tol);
                    end
                    fields = fieldnames(constraints);
                    for ff=1:length(fields)
                        testCase.verifyEqual(constraints.(fields{ff}),constraintsy.(fields{ff}),'reltol',testCase.tol);
                    end
                case 'tonly'
                     testCase.verifyEqual(optvars.t,optvarsy.t,'reltol',testCase.tol);
                otherwise
                    error('unknown check type')
            end
            % display the difference between the optimisation variables
            fprintf('No=%d Ni=%d N=%d \n   t_nonsdp: %s t_yalmip: %s \n   times: nonsdp: %ss yalmip %ss  \n',No,Ni,N,num2str(optvars.t),num2str(optvarsy.t),num2str(tnonsdp),num2str(tyalmip));
        end
    end
end
