function [Ap,Bp,Cp,Dp,optvars,constraints] = makepassive_yalmip(A,B,C,D,varargin)
% MAKEPASSIVE_YALMIP uses the yalmip optimisation to perform a passive approximation
%
%   [Ap,Bp,Cp,Dp,optvars,constraints] = makepassive_yalmip(A,B,C,D)
%
%
% This function requires yalmip with the SPDT solver. 
% You can download these at
%
%   https://yalmip.github.io/download/
%   http://www.math.nus.edu.sg/~mattohkc/sdpt3.html

if nargin>=4
    [N,No,Ni]=validateABCD(A,B,C,D);
else
    error('You need to specify the A,B,C and D matrices');
end

p=inputParser();
% domain of the state-space system
p.addParameter('Domain','DISC',@(x) any(strcmpi(x,{'PLANE','DISC'})));
% Frequency normalisation used on the system
p.addParameter('Normalisation',LowpassNormalisation(-1,1),@(x) isa(x,'Normalisation'));
% Transform to map the system from the complex plane to the disc
p.addParameter('Transform',MobiusTransform('Inside','LHP'),@(x) isa(x,'Transform'));
% set this to true to show the nonsdp info
p.addParameter('Display',false,@islogical);
% nonsdp solver options. When you set this, the display option is
% overwritten by whatever nonopt.Display has
p.addParameter('nonopt',[],@(x) isa(x,'nonopt'));
p.parse(varargin{:});
args = p.Results;

% go to the disc if needed
if strcmpi(args.Domain,'PLANE')
    % normalise
    [A,B,C,D]=Forward_ABCD(args.Normalisation,A,B,C,D);
    % go to the disc
    [A,B,C,D]=Forward_ABCD(args.Transform,A,B,C,D);
end

% check whether the system has all its poles inside the unit circle
if any(abs(eig(A))>=1)
    error('The system has poles outside of the unit circle');
end

% balance the system for good numerical conditioning
[A,B,C,D,Winv] = balanceSystem(A,B,C,D);

% check whether the system is real or complex
if isreal([A B;C D])
    type = 'real';
else
    type = 'complex';
end

% create the optimisation variables
t  = sdpvar(1,1);
Bp = sdpvar(N   ,Ni  ,'full'     ,type);
Dp = sdpvar(No  ,Ni  ,'full'     ,type);
P  = sdpvar(N   ,N   ,'hermitian',type);
Q  = sdpvar(N+No,N+No,'hermitian',type);
Z  = sdpvar(Ni  ,Ni  ,'hermitian',type);

% create the constraints
F = [P>=0
    [Q [Bp;Dp];[Bp;Dp]' eye(Ni)]>=0
    [P-A*P*(A') -A*P*(C');-C*P*(A') eye(No)-C*P*(C')]-Q  >= 0
    [Z [B;D]'-[Bp;Dp]';[B;D]-[Bp;Dp] t.*[Winv zeros(N,No);zeros(No,N) eye(No)]] >=0
    1-trace(Z)>=0];
% set some yalmip options
ops = sdpsettings('solver','sdpt3','verbose',0);
% call the optimisation
res = optimize(F,t,ops);

% check for errors
if res.problem~=0
    disp(res);
    error('Optimisation did not work');
end

% get the values out of the optimisation struct
Bp = value(Bp);
Dp = value(Dp);
Ap=A;
Cp=C;

optvars.t = value(t);
optvars.B = B;
optvars.D = D;
optvars.Q = value(Q);
optvars.Z = value(Z);
optvars.P = value(P);

constraints.Ppos  = value(F(1));
constraints.Qpos  = value(F(2));
constraints.PQpos = value(F(3));
constraints.Zpos  = value(F(4));
constraints.traZ  = value(F(5));

% go to the disc if needed
if strcmpi(args.Domain,'PLANE')
    % go to the plane
    [Ap,Bp,Cp,Dp]=Backward_ABCD(args.Transform,Ap,Bp,Cp,Dp);
    % normalise
    [Ap,Bp,Cp,Dp]=Backward_ABCD(args.Normalisation,Ap,Bp,Cp,Dp);
end

end


function [N,No,Ni]=validateABCD(A,B,C,D)
validateattributes(A,{'numeric'},{'square','finite','nonnan'});
N = size(A,1);
validateattributes(B,{'numeric'},{'size',[N NaN],'finite','nonnan'});
No = size(C,1);
validateattributes(C,{'numeric'},{'size',[NaN N],'finite','nonnan'});
Ni = size(B,2);
validateattributes(D,{'numeric'},{'size',[No Ni],'finite','nonnan'});
end
